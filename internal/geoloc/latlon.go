package geoloc

import (
	"fmt"
	"log"
	"strings"
)

// LatLonDeg represents a geographic coordinate in decimal degrees.
// Latitude and Longitude are stored as float64 values.
type LatLonDeg struct {
	Latitude, Longitude float64
}

func (a *LatLonDeg) String() string {
	return fmt.Sprintf("Latitude=%.6f Longitude=%.6f", a.Latitude, a.Longitude)
}

func (a *LatLonDeg) equal(b LatLonDeg) bool {
	return a.String() == b.String()
}

// LatLonChar Maidenhead Encoded
type LatLonChar struct {
	LatChar, LonChar byte
}

func isNumber(b byte) bool {
	return b > 47 && b < 58
}

func isLetter(b byte) bool {
	return b > 64 && b < 91
}

func toValidValue(c string) byte {
	if len(c) != 1 {
		log.Printf("Illegal argument, only one character is accepted! argumetn=%s", c)
		return 32 // empty string
	}
	c = strings.ToUpper(c)
	b := c[0]
	if isLetter(b) || isNumber(b) {
		return b
	} else {
		log.Printf("Illegal character:%s", c)
		return 32
	}
}

func (a *LatLonChar) setLatChar(c string) {
	a.LatChar = toValidValue(c)
}

func (a *LatLonChar) setLonChar(c string) {
	a.LonChar = toValidValue(c)
}

func (a *LatLonChar) GetLatChar() string {
	return string(a.LatChar)
}

func (a *LatLonChar) GetLonChar() string {
	return string(a.LonChar)
}

func (a *LatLonChar) isSet() bool {
	return a.LatChar > 0 && a.LonChar > 0
}

func (a *LatLonChar) String() string {
	if a.isSet() {
		return fmt.Sprintf("%s%s", a.GetLonChar(), a.GetLatChar())
	} else {
		return ""
	}
}

func (a *LatLonChar) equal(b LatLonChar) bool {
	return a.String() == b.String()
}
