package geoloc

import (
	"reflect"
	"testing"
)

func (a *Square) equals(b Square) bool {
	return a.Encoded.equal(b.Encoded) && a.Decoded.equal(b.Decoded)
}

func TestSquare_String(t *testing.T) {
	type fields struct {
		decoded LatLonDeg
		encoded LatLonChar
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name:   "toString-zero-1",
			fields: fields{},
			want:   "Decoded:Latitude=0.000000 Longitude=0.000000",
		},
		{
			name: "toString-Decoded-zero-2",
			fields: fields{
				decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 0,
				},
			},
			want: "Decoded:Latitude=0.000000 Longitude=0.000000",
		},
		{
			name: "toString-zero-3",
			fields: fields{
				decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 0,
				},
				encoded: LatLonChar{
					LatChar: 0,
					LonChar: 0,
				},
			},
			want: "Decoded:Latitude=0.000000 Longitude=0.000000",
		},
		{
			name: "toString-Decoded-1",
			fields: fields{
				decoded: LatLonDeg{
					Latitude:  1,
					Longitude: 2,
				},
			},
			want: "Decoded:Latitude=1.000000 Longitude=2.000000",
		},
		{
			name: "toString-Decoded-2",
			fields: fields{
				decoded: LatLonDeg{
					Latitude:  9,
					Longitude: 18,
				},
			},
			want: "Decoded:Latitude=9.000000 Longitude=18.000000",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &Square{
				Decoded: tt.fields.decoded,
				Encoded: tt.fields.encoded,
			}
			if got := a.String(); got != tt.want {
				t.Errorf("Square.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSquare_Equals(t *testing.T) {
	type fields struct {
		decoded LatLonDeg
		encoded LatLonChar
	}
	type args struct {
		b Square
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "equals-1",
			fields: fields{
				decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 0,
				},
				encoded: LatLonChar{
					LatChar: 0,
					LonChar: 0,
				},
			},
			args: args{
				b: Square{
					Decoded: LatLonDeg{
						Latitude:  0,
						Longitude: 0,
					},
					Encoded: LatLonChar{
						LatChar: 0,
						LonChar: 0,
					},
				},
			},
			want: true,
		},
		{
			name: "equals-2",
			fields: fields{
				decoded: LatLonDeg{
					Latitude:  1,
					Longitude: 2,
				},
				encoded: LatLonChar{
					LatChar: 48,
					LonChar: 49,
				},
			},
			args: args{
				b: Square{
					Decoded: LatLonDeg{
						Latitude:  0,
						Longitude: 0,
					},
					Encoded: LatLonChar{
						LatChar: 0,
						LonChar: 0,
					},
				},
			},
			want: false,
		},
		{
			name: "equals-3",
			fields: fields{
				decoded: LatLonDeg{
					Latitude:  1,
					Longitude: 2,
				},
				encoded: LatLonChar{
					LatChar: 48,
					LonChar: 49,
				},
			},
			args: args{
				b: Square{
					Decoded: LatLonDeg{
						Latitude:  1,
						Longitude: 2,
					},
					Encoded: LatLonChar{
						LatChar: 0,
						LonChar: 0,
					},
				},
			},
			want: false,
		},

		{
			name: "equals-4",
			fields: fields{
				decoded: LatLonDeg{
					Latitude:  1,
					Longitude: 2,
				},
				encoded: LatLonChar{
					LatChar: 48,
					LonChar: 49,
				},
			},
			args: args{
				b: Square{
					Decoded: LatLonDeg{
						Latitude:  1,
						Longitude: 2,
					},
					Encoded: LatLonChar{
						LatChar: 48,
						LonChar: 49,
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &Square{
				Decoded: tt.fields.decoded,
				Encoded: tt.fields.encoded,
			}
			if got := a.equals(tt.args.b); got != tt.want {
				t.Errorf("Square.equals() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSquareEncode(t *testing.T) {
	type args struct {
		lld LatLonDeg
	}
	tests := []struct {
		name  string
		args  args
		want  Field
		want1 Square
	}{
		{
			name: "encode-JJ00-1",
			args: args{
				lld: LatLonDeg{
					Latitude:  0,
					Longitude: 0,
				},
			},
			want: Field{
				Decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 0,
				},
				Encoded: LatLonChar{
					LatChar: 74,
					LonChar: 74,
				},
			},
			want1: Square{
				Decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 0,
				},
				Encoded: LatLonChar{
					LatChar: 48,
					LonChar: 48,
				},
			},
		},

		{
			name: "encode-JJ00-2",
			args: args{
				lld: LatLonDeg{
					Latitude:  0.0001,
					Longitude: 0.0001,
				},
			},
			want: Field{
				Decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 0,
				},
				Encoded: LatLonChar{
					LatChar: 74,
					LonChar: 74,
				},
			},
			want1: Square{
				Decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 0,
				},
				Encoded: LatLonChar{
					LatChar: 48,
					LonChar: 48,
				},
			},
		},

		{
			name: "encode-JJ00-3",
			args: args{
				lld: LatLonDeg{
					Latitude:  0.01,
					Longitude: 0.01,
				},
			},
			want: Field{
				Decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 0,
				},
				Encoded: LatLonChar{
					LatChar: 74,
					LonChar: 74,
				},
			},
			want1: Square{
				Decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 0,
				},
				Encoded: LatLonChar{
					LatChar: 48,
					LonChar: 48,
				},
			},
		},

		{
			name: "encode-JJ99-4",
			args: args{
				lld: LatLonDeg{
					Latitude:  9.99,
					Longitude: 19.99,
				},
			},
			want: Field{
				Decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 0,
				},
				Encoded: LatLonChar{
					LatChar: 74,
					LonChar: 74,
				},
			},
			want1: Square{
				Decoded: LatLonDeg{
					Latitude:  9,
					Longitude: 18,
				},
				Encoded: LatLonChar{
					LatChar: 57,
					LonChar: 57,
				},
			},
		},

		{
			name: "encode-KK00-5",
			args: args{
				lld: LatLonDeg{
					Latitude:  10,
					Longitude: 20,
				},
			},
			want: Field{
				Decoded: LatLonDeg{
					Latitude:  10,
					Longitude: 20,
				},
				Encoded: LatLonChar{
					LatChar: 75,
					LonChar: 75,
				},
			},
			want1: Square{
				Decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 0,
				},
				Encoded: LatLonChar{
					LatChar: 48,
					LonChar: 48,
				},
			},
		},

		{
			name: "encode-AA00-5",
			args: args{
				lld: LatLonDeg{
					Latitude:  -90,
					Longitude: -180,
				},
			},
			want: Field{
				Decoded: LatLonDeg{
					Latitude:  -90,
					Longitude: -180,
				},
				Encoded: LatLonChar{
					LatChar: 65,
					LonChar: 65,
				},
			},
			want1: Square{
				Decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 0,
				},
				Encoded: LatLonChar{
					LatChar: 48,
					LonChar: 48,
				},
			},
		},

		{
			name: "encode-AA00-6",
			args: args{
				lld: LatLonDeg{
					Latitude:  -89.99,
					Longitude: -179.99,
				},
			},
			want: Field{
				Decoded: LatLonDeg{
					Latitude:  -90,
					Longitude: -180,
				},
				Encoded: LatLonChar{
					LatChar: 65,
					LonChar: 65,
				},
			},
			want1: Square{
				Decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 0,
				},
				Encoded: LatLonChar{
					LatChar: 48,
					LonChar: 48,
				},
			},
		},

		{
			name: "encode-AA99-7",
			args: args{
				lld: LatLonDeg{
					Latitude:  -80.01,
					Longitude: -160.01,
				},
			},
			want: Field{
				Decoded: LatLonDeg{
					Latitude:  -90,
					Longitude: -180,
				},
				Encoded: LatLonChar{
					LatChar: 65,
					LonChar: 65,
				},
			},
			want1: Square{
				Decoded: LatLonDeg{
					Latitude:  9,
					Longitude: 18,
				},
				Encoded: LatLonChar{
					LatChar: 57,
					LonChar: 57,
				},
			},
		},

		{
			name: "encode-BB00-7",
			args: args{
				lld: LatLonDeg{
					Latitude:  -80,
					Longitude: -160,
				},
			},
			want: Field{
				Decoded: LatLonDeg{
					Latitude:  -80,
					Longitude: -160,
				},
				Encoded: LatLonChar{
					LatChar: 66,
					LonChar: 66,
				},
			},
			want1: Square{
				Decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 0,
				},
				Encoded: LatLonChar{
					LatChar: 48,
					LonChar: 48,
				},
			},
		},

		{
			name: "encode-S59ABC-JN76TO",
			args: args{
				lld: LatLonDeg{
					Latitude:  46.3,
					Longitude: 15.3,
				},
			},
			want: Field{
				Decoded: LatLonDeg{
					Latitude:  40,
					Longitude: 0,
				},
				Encoded: LatLonChar{
					LatChar: 78,
					LonChar: 74,
				},
			},
			want1: Square{
				Decoded: LatLonDeg{
					Latitude:  6,
					Longitude: 14,
				},
				Encoded: LatLonChar{
					LatChar: 54,
					LonChar: 55,
				},
			},
		},

		{
			name: "encode-K1TTT-FN32LL",
			args: args{
				lld: LatLonDeg{
					Latitude:  42.4662,
					Longitude: -73.0232,
				},
			},
			want: Field{
				Decoded: LatLonDeg{
					Latitude:  40,
					Longitude: -80,
				},
				Encoded: LatLonChar{
					LatChar: 78,
					LonChar: 70,
				},
			},
			want1: Square{
				Decoded: LatLonDeg{
					Latitude:  2,
					Longitude: 6,
				},
				Encoded: LatLonChar{
					LatChar: 50,
					LonChar: 51,
				},
			},
		},

		{
			name: "encode-PS2T-GG58WG",
			args: args{
				lld: LatLonDeg{
					Latitude:  -21.7487,
					Longitude: -48.1268,
				},
			},
			want: Field{
				Decoded: LatLonDeg{
					Latitude:  -30,
					Longitude: -60,
				},
				Encoded: LatLonChar{
					LatChar: 71,
					LonChar: 71,
				},
			},
			want1: Square{
				Decoded: LatLonDeg{
					Latitude:  8,
					Longitude: 10,
				},
				Encoded: LatLonChar{
					LatChar: 56,
					LonChar: 53,
				},
			},
		},

		{
			name: "encode-ZM4T-RF80LQ",
			args: args{
				lld: LatLonDeg{
					Latitude:  -39.3125,
					Longitude: 176.9583333,
				},
			},
			want: Field{
				Decoded: LatLonDeg{
					Latitude:  -40,
					Longitude: 160,
				},
				Encoded: LatLonChar{
					LatChar: 70,
					LonChar: 82,
				},
			},
			want1: Square{
				Decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 16,
				},
				Encoded: LatLonChar{
					LatChar: 48,
					LonChar: 56,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := SquareEncode(tt.args.lld)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FielsEncode() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("SquareEncode() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestSquareDecode(t *testing.T) {
	type args struct {
		f         Field
		squareLLC LatLonChar
	}
	tests := []struct {
		name string
		args args
		want Square
	}{
		{
			name: "decode-zero",
			args: args{
				f: Field{
					Decoded: LatLonDeg{
						Latitude:  0,
						Longitude: 0,
					},
					Encoded: LatLonChar{
						LatChar: 0,
						LonChar: 0,
					},
				},
				squareLLC: LatLonChar{
					LatChar: 0,
					LonChar: 0,
				},
			},
			want: Square{
				Decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 0,
				},
				Encoded: LatLonChar{
					LatChar: 0,
					LonChar: 0,
				},
			},
		},

		{
			name: "decode-JJ00",
			args: args{
				f: Field{
					Encoded: LatLonChar{
						LatChar: 74,
						LonChar: 74,
					},
				},
				squareLLC: LatLonChar{
					LatChar: 48,
					LonChar: 48,
				},
			},
			want: Square{
				Decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 0,
				},
				Encoded: LatLonChar{
					LatChar: 48,
					LonChar: 48,
				},
			},
		},

		{
			name: "decode-AA00",
			args: args{
				f: Field{
					Encoded: LatLonChar{
						LatChar: 65,
						LonChar: 65,
					},
				},
				squareLLC: LatLonChar{
					LatChar: 48,
					LonChar: 48,
				},
			},
			want: Square{
				Decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 0,
				},
				Encoded: LatLonChar{
					LatChar: 48,
					LonChar: 48,
				},
			},
		},

		{
			name: "decode-JJ99",
			args: args{
				f: Field{
					Encoded: LatLonChar{
						LatChar: 74,
						LonChar: 74,
					},
				},
				squareLLC: LatLonChar{
					LatChar: 57,
					LonChar: 57,
				},
			},
			want: Square{
				Decoded: LatLonDeg{
					Latitude:  9,
					Longitude: 18,
				},
				Encoded: LatLonChar{
					LatChar: 57,
					LonChar: 57,
				},
			},
		},

		{
			name: "decode-AA99",
			args: args{
				f: Field{
					Encoded: LatLonChar{
						LatChar: 65,
						LonChar: 65,
					},
				},
				squareLLC: LatLonChar{
					LatChar: 57,
					LonChar: 57,
				},
			},
			want: Square{
				Decoded: LatLonDeg{
					Latitude:  9,
					Longitude: 18,
				},
				Encoded: LatLonChar{
					LatChar: 57,
					LonChar: 57,
				},
			},
		},

		{
			name: "decode-S59ABC-JN76TO",
			args: args{
				f: Field{
					Encoded: LatLonChar{
						LatChar: 78,
						LonChar: 74,
					},
				},
				squareLLC: LatLonChar{
					LatChar: 54,
					LonChar: 55,
				},
			},
			want: Square{
				Decoded: LatLonDeg{
					Latitude:  6,
					Longitude: 14,
				},
				Encoded: LatLonChar{
					LatChar: 54,
					LonChar: 55,
				},
			},
		},

		{
			name: "decode-K1TTT-FN32LL",
			args: args{
				f: Field{
					Encoded: LatLonChar{
						LatChar: 78, // N
						LonChar: 70, // F
					},
				},
				squareLLC: LatLonChar{
					LatChar: 50, // 2
					LonChar: 51, // 3
				},
			},
			want: Square{
				Decoded: LatLonDeg{
					Latitude:  2,
					Longitude: 6,
				},
				Encoded: LatLonChar{
					LatChar: 50,
					LonChar: 51,
				},
			},
		},

		{
			name: "decode--JN32",
			args: args{
				f: Field{
					Encoded: LatLonChar{
						LatChar: 78, // N
						LonChar: 74, // J
					},
				},
				squareLLC: LatLonChar{
					LatChar: 50, // 2
					LonChar: 51, // 3
				},
			},
			want: Square{
				Decoded: LatLonDeg{
					Latitude:  2,
					Longitude: 6,
				},
				Encoded: LatLonChar{
					LatChar: 50,
					LonChar: 51,
				},
			},
		},

		{
			name: "decode-PS2T-GG58WG",
			args: args{
				f: Field{
					Encoded: LatLonChar{
						LatChar: 71, // G
						LonChar: 71, // G
					},
				},
				squareLLC: LatLonChar{
					LatChar: 56, // 8
					LonChar: 53, // 5
				},
			},
			want: Square{
				Decoded: LatLonDeg{
					Latitude:  8,
					Longitude: 10,
				},
				Encoded: LatLonChar{
					LatChar: 56,
					LonChar: 53,
				},
			},
		},

		{
			name: "decode-ZM4T-RF80LQ",
			args: args{
				f: Field{
					Encoded: LatLonChar{
						LatChar: 70, // F
						LonChar: 82, // R
					},
				},
				squareLLC: LatLonChar{
					LatChar: 48, // 0
					LonChar: 56, // 8
				},
			},
			want: Square{
				Decoded: LatLonDeg{
					Latitude:  0,
					Longitude: 16,
				},
				Encoded: LatLonChar{
					LatChar: 48,
					LonChar: 56,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SquareDecode(tt.args.squareLLC); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SquareDecode() = %v, want %v", got, tt.want)
			}
		})
	}
}
