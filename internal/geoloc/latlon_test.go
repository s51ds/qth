package geoloc

import (
	"fmt"
	"testing"
)

func TestLatLon_Equal(t *testing.T) {
	a := LatLonDeg{}
	b := LatLonDeg{}
	if !a.equal(b) {
		t.Fatal()
	}
	//
	a.Longitude = 1.1
	a.Latitude = 2.2
	if a.equal(b) {
		t.Fatal()
	}
	//
	b.Longitude = 1.1
	b.Latitude = 2.2
	if !a.equal(b) {
		t.Fatal()
	}
}

func TestLatLon_String(t *testing.T) {
	a := LatLonDeg{1.12345, -1.12345}
	if a.String() != "Latitude=1.123450 Longitude=-1.123450" {
		fmt.Println(a.String())
		t.Fatal()
	}
}

func TestLatLonChar_LatLonChar(t *testing.T) {
	a := LatLonChar{}
	a.setLatChar("A")
	a.setLonChar("B")
	if a.String() != "BA" {
		t.Fatal()
	}
	a.setLatChar("a")
	a.setLonChar("b")
	if a.String() != "BA" {
		t.Fatal()
	}

	a.setLatChar("AA")
	a.setLonChar("BB")
	if a.String() != "  " {
		t.Fatal()
	}
	a.setLatChar("0")
	a.setLonChar("9")
	if a.String() != "90" {
		t.Fatal()
	}
	a.setLatChar("!")
	a.setLonChar("9")
	if a.String() != "9 " {
		t.Fatal()
	}
	fmt.Println(a.String())
}

func TestLatLonChar_Equal(t *testing.T) {
	a := LatLonChar{}
	if !a.equal(a) {
		t.Fatal()
	}
	b := LatLonChar{}
	b.setLatChar("J")
	b.setLonChar("N")
	if a.equal(b) {
		t.Fatal()
	}
	a.setLatChar("J")
	a.setLonChar("N")
	if !a.equal(b) {
		t.Fatal()
	}

}
