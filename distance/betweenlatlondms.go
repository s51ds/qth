package distance

import (
	"gitlab.com/s51ds/qth/location"
)

// BetweenLatLonDMS calculates the distance (in kilometers) and azimuth (in degrees) between two geographical points given in DMS format.
// It returns an error if the input coordinates are invalid.
func BetweenLatLonDMS(latLonDmsA, latLonDmsB location.LatLonDMS) (km, azimuth float64, err error) {
	latLonDdA, err := location.ConvertLatLonDMStoLatLonDD(latLonDmsA)
	if err != nil {
		return 0, 0, err
	}
	latLonDdB, err := location.ConvertLatLonDMStoLatLonDD(latLonDmsB)
	if err != nil {
		return 0, 0, err
	}

	return BetweenLatLonDD(latLonDdA, latLonDdB)
}
