package distance

import (
	"fmt"
	"testing"

	"gitlab.com/s51ds/qth/location"
)

func TestBetweenLatLon(t *testing.T) {
	type args struct {
		latLonA location.LatLonDD
		latLonB location.LatLonDD
	}
	tests := []struct {
		name        string
		args        args
		wantKm      float64
		wantAzimuth float64
		wantErr     bool
	}{
		// ///////////////////////
		//  https://www.movable-type.co.uk/scripts/latlong.html
		{
			name: "Point-1 to Point-2",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  50.066389,
					Longitude: -5.714722,
				},
				latLonB: location.LatLonDD{
					Latitude:  58.643889,
					Longitude: -3.07,
				},
			},
			wantKm:      968.8548823543566,
			wantAzimuth: 9.119817327410368,
			wantErr:     false,
		},
		{
			name: "Point-2 to Point-1",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  58.643889,
					Longitude: -3.07,
				},
				latLonB: location.LatLonDD{
					Latitude:  50.066389,
					Longitude: -5.714722,
				},
			},
			wantKm:      968.8548823543566,
			wantAzimuth: 191.27520031620156,
			wantErr:     false,
		},
		{
			name: "Baghdad to Osaka",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  35,
					Longitude: 45,
				},
				latLonB: location.LatLonDD{
					Latitude:  35,
					Longitude: 135,
				},
			},
			wantKm:      7871.77997187436,
			wantAzimuth: 60.16243352168622,
			wantErr:     false,
		},
		{
			name: "Osaka to Baghdad",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  35,
					Longitude: 135,
				},
				latLonB: location.LatLonDD{
					Latitude:  35,
					Longitude: 45,
				},
			},
			wantKm:      7871.77997187436,
			wantAzimuth: 299.83756647831376,
			wantErr:     false,
		},

		// //////////////////////
		// https://edwilliams.org/avform147.htm#Intro
		{
			name: "LAX to JFK",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  33.95,
					Longitude: -118.4,
				},
				latLonB: location.LatLonDD{
					Latitude:  40.633333,
					Longitude: -73.783333,
				},
			},
			wantKm:      3972.863294353866,
			wantAzimuth: 65.8921670931295,
			wantErr:     false,
		},
		{
			name: "JFK to LAX",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  40.633333,
					Longitude: -73.783333,
				},
				latLonB: location.LatLonDD{
					Latitude:  33.95,
					Longitude: -118.4,
				},
			},
			wantKm:      3972.863294353866,
			wantAzimuth: 273.8581644724343,
			wantErr:     false,
		},

		//
		// /////////////////////

		// ////////////////////////////////////
		// https://www.nhc.noaa.gov/gccalc.shtml
		//
		{
			name: "0.0N 0.0W to 0.0N 0.0W",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				latLonB: location.LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
			},
			wantKm:      0,
			wantAzimuth: 0,
			wantErr:     false,
		},
		{
			name: "1.0N 0.0W to 0.0N 0.0W",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  1,
					Longitude: 0,
				},
				latLonB: location.LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
			},
			wantKm:      111.1950802335329,
			wantAzimuth: 180,
			wantErr:     false,
		},

		//
		// //////////////////////////////////////////
		//
		{
			name: "1.0-0.0",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  1,
					Longitude: 0,
				},
				latLonB: location.LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
			},
			wantKm:      111.1950802335329,
			wantAzimuth: 180,
			wantErr:     false,
		},
		{
			name: "0.0-1.0",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				latLonB: location.LatLonDD{
					Latitude:  1,
					Longitude: 0,
				},
			},
			wantKm:      111.1950802335329,
			wantAzimuth: 0,
			wantErr:     false,
		},
		//
		{
			name: "1.1-0.0",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  1,
					Longitude: 1,
				},
				latLonB: location.LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
			},
			wantKm:      157.2495984740402,
			wantAzimuth: 225.00436354465515,
			wantErr:     false,
		},
		{
			name: "0.0-1.1",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				latLonB: location.LatLonDD{
					Latitude:  1,
					Longitude: 1,
				},
			},
			wantKm:      157.2495984740402,
			wantAzimuth: 44.99563645534485,
			wantErr:     false,
		},
		//
		{
			name: "0.1-0.0",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  0,
					Longitude: 1,
				},
				latLonB: location.LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
			},
			wantKm:      111.1950802335329,
			wantAzimuth: 270,
			wantErr:     false,
		},
		{
			name: "0.0-0.1",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				latLonB: location.LatLonDD{
					Latitude:  0,
					Longitude: 1,
				},
			},
			wantKm:      111.1950802335329,
			wantAzimuth: 90,
			wantErr:     false,
		},
		//
		{
			name: "1.1--1.-1",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  1,
					Longitude: 1,
				},
				latLonB: location.LatLonDD{
					Latitude:  -1,
					Longitude: -1,
				},
			},
			wantKm:      314.49919694808045,
			wantAzimuth: 225.00436354465515,
			wantErr:     false,
		},
		{
			name: "-1.-1-1.1",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  -1,
					Longitude: -1,
				},
				latLonB: location.LatLonDD{
					Latitude:  1,
					Longitude: 1,
				},
			},
			wantKm:      314.49919694808045,
			wantAzimuth: 45.00436354465515,
			wantErr:     false,
		},
		{
			name: "Latitude limit case",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  90,
					Longitude: 0,
				},
				latLonB: location.LatLonDD{
					Latitude:  -90,
					Longitude: 0,
				},
			},
			wantKm:      20015.11444203,
			wantAzimuth: 180,
			wantErr:     false,
		},
		{
			name: "Longitude limit case",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  0,
					Longitude: 180,
				},
				latLonB: location.LatLonDD{
					Latitude:  0,
					Longitude: -180,
				},
			},
			wantKm:      0,
			wantAzimuth: 90,
			wantErr:     false,
		},
		{
			name: "Invalid Latitude A",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  200,
					Longitude: 0,
				},
				latLonB: location.LatLonDD{
					Latitude:  -90,
					Longitude: 0,
				},
			},
			wantKm:      0,
			wantAzimuth: 0,
			wantErr:     true,
		},
		{
			name: "Invalid Latitude B",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  90,
					Longitude: 0,
				},
				latLonB: location.LatLonDD{
					Latitude:  190,
					Longitude: -180,
				},
			},
			wantKm:      0,
			wantAzimuth: 0,
			wantErr:     true,
		},
		{
			name: "Max distance and bearing at 45 degrees",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  90,
					Longitude: 0,
				},
				latLonB: location.LatLonDD{
					Latitude:  -90,
					Longitude: 0,
				},
			},
			wantKm:      20015.11444203,
			wantAzimuth: 180,
			wantErr:     false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotKm, gotAzimuth, err := BetweenLatLonDD(tt.args.latLonA, tt.args.latLonB)
			if (err != nil) != tt.wantErr {
				t.Errorf("BetweenLatLonDD() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if fmt.Sprintf("%.2f", gotKm) != fmt.Sprintf("%.2f", tt.wantKm) {
				t.Errorf("BetweenLatLonDD() gotKm = %v, want %v", gotKm, tt.wantKm)
			}
			if fmt.Sprintf("%.2f", gotAzimuth) != fmt.Sprintf("%.2f", tt.wantAzimuth) {
				t.Errorf("BetweenLatLonDD() gotAzimuth = %v, want %v", gotAzimuth, tt.wantAzimuth)
			}

		})
	}
}

func TestBetweenLatLonKm(t *testing.T) {
	type args struct {
		latLonA location.LatLonDD
		latLonB location.LatLonDD
	}
	tests := []struct {
		name   string
		args   args
		wantKm float64
	}{
		// ///////////////////////
		//  https://www.movable-type.co.uk/scripts/latlong.html
		{
			name: "Point-1 to Point-2",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  50.066389,
					Longitude: -5.714722,
				},
				latLonB: location.LatLonDD{
					Latitude:  58.643889,
					Longitude: -3.07,
				},
			},
			wantKm: 968.8548823543566,
		},
		{
			name: "Point-2 to Point-1",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  58.643889,
					Longitude: -3.07,
				},
				latLonB: location.LatLonDD{
					Latitude:  50.066389,
					Longitude: -5.714722,
				},
			},
			wantKm: 968.8548823543566,
		},
		{
			name: "Baghdad to Osaka",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  35,
					Longitude: 45,
				},
				latLonB: location.LatLonDD{
					Latitude:  35,
					Longitude: 135,
				},
			},
			wantKm: 7871.77997187436,
		},
		{
			name: "Osaka to Baghdad",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  35,
					Longitude: 135,
				},
				latLonB: location.LatLonDD{
					Latitude:  35,
					Longitude: 45,
				},
			},
			wantKm: 7871.77997187436,
		},

		// //////////////////////
		// https://edwilliams.org/avform147.htm#Intro
		{
			name: "LAX to JFK",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  33.95,
					Longitude: -118.4,
				},
				latLonB: location.LatLonDD{
					Latitude:  40.633333,
					Longitude: -73.783333,
				},
			},
			wantKm: 3972.863294353866,
		},
		{
			name: "JFK to LAX",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  40.633333,
					Longitude: -73.783333,
				},
				latLonB: location.LatLonDD{
					Latitude:  33.95,
					Longitude: -118.4,
				},
			},
			wantKm: 3972.863294353866,
		},

		//
		// /////////////////////

		// ////////////////////////////////////
		// https://www.nhc.noaa.gov/gccalc.shtml
		//
		{
			name: "0.0N 0.0W to 0.0N 0.0W",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				latLonB: location.LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
			},
			wantKm: 0,
		},
		{
			name: "1.0N 0.0W to 0.0N 0.0W",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  1,
					Longitude: 0,
				},
				latLonB: location.LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
			},
			wantKm: 111.1950802335329,
		},

		//
		// //////////////////////////////////////////
		//
		{
			name: "1.0-0.0",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  1,
					Longitude: 0,
				},
				latLonB: location.LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
			},
			wantKm: 111.1950802335329,
		},
		{
			name: "0.0-1.0",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				latLonB: location.LatLonDD{
					Latitude:  1,
					Longitude: 0,
				},
			},
			wantKm: 111.1950802335329,
		},
		//
		{
			name: "1.1-0.0",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  1,
					Longitude: 1,
				},
				latLonB: location.LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
			},
			wantKm: 157.2495984740402,
		},
		{
			name: "0.0-1.1",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				latLonB: location.LatLonDD{
					Latitude:  1,
					Longitude: 1,
				},
			},
			wantKm: 157.2495984740402,
		},
		//
		{
			name: "0.1-0.0",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  0,
					Longitude: 1,
				},
				latLonB: location.LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
			},
			wantKm: 111.1950802335329,
		},
		{
			name: "0.0-0.1",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				latLonB: location.LatLonDD{
					Latitude:  0,
					Longitude: 1,
				},
			},
			wantKm: 111.1950802335329,
		},
		//
		{
			name: "1.1--1.-1",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  1,
					Longitude: 1,
				},
				latLonB: location.LatLonDD{
					Latitude:  -1,
					Longitude: -1,
				},
			},
			wantKm: 314.49919694808045,
		},
		{
			name: "-1.-1-1.1",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  -1,
					Longitude: -1,
				},
				latLonB: location.LatLonDD{
					Latitude:  1,
					Longitude: 1,
				},
			},
			wantKm: 314.49919694808045,
		},
		{
			name: "Latitude limit case",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  90,
					Longitude: 0,
				},
				latLonB: location.LatLonDD{
					Latitude:  -90,
					Longitude: 0,
				},
			},
			wantKm: 20015.11444203,
		},
		{
			name: "Longitude limit case",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  0,
					Longitude: 180,
				},
				latLonB: location.LatLonDD{
					Latitude:  0,
					Longitude: -180,
				},
			},
			wantKm: 0,
		},
		{
			name: "Max distance and bearing at 45 degrees",
			args: args{
				latLonA: location.LatLonDD{
					Latitude:  90,
					Longitude: 0,
				},
				latLonB: location.LatLonDD{
					Latitude:  -90,
					Longitude: 0,
				},
			},
			wantKm: 20015.11444203,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotKm := BetweenLatLonDDkm(tt.args.latLonA, tt.args.latLonB)
			if fmt.Sprintf("%.2f", gotKm) != fmt.Sprintf("%.2f", tt.wantKm) {
				t.Errorf("BetweenLatLonDD() gotKm = %v, want %v", gotKm, tt.wantKm)
			}
		})
	}
}

func BenchmarkBetweenLatLonDD(b *testing.B) {
	latLonA := location.LatLonDD{Latitude: 46.0511, Longitude: 14.5051} // Ljubljana
	latLonB := location.LatLonDD{Latitude: 48.8566, Longitude: 2.3522}  // Paris

	for i := 0; i < b.N; i++ {
		_, _, _ = BetweenLatLonDD(latLonA, latLonB)
	}
}

func BenchmarkBetweenLatLonDDkm(b *testing.B) {
	latLonA := location.LatLonDD{Latitude: 46.0511, Longitude: 14.5051} // Ljubljana
	latLonB := location.LatLonDD{Latitude: 48.8566, Longitude: 2.3522}  // Paris

	for i := 0; i < b.N; i++ {
		_ = BetweenLatLonDDkm(latLonA, latLonB)
	}
}
