package distance

import (
	"testing"

	"gitlab.com/s51ds/qth/internal/geoloc"
	"gitlab.com/s51ds/qth/location"
)

func TestBetweenDMS(t *testing.T) {
	type args struct {
		latLonDmsA location.LatLonDMS
		latLonDmsB location.LatLonDMS
	}
	tests := []struct {
		name        string
		args        args
		wantKm      float64
		wantAzimuth float64
		wantErr     bool
	}{

		{
			name: "0",
			args: args{
				latLonDmsA: location.LatLonDMS{
					Latitude: location.DMS{
						Degrees: 0,
						Minutes: 0,
						Seconds: 0,
					},
					Longitude: location.DMS{
						Degrees: 0,
						Minutes: 0,
						Seconds: 0,
					},
				},
				latLonDmsB: location.LatLonDMS{
					Latitude: location.DMS{
						Degrees: 0,
						Minutes: 0,
						Seconds: 0,
					},
					Longitude: location.DMS{
						Degrees: 0,
						Minutes: 0,
						Seconds: 0,
					},
				},
			},
			wantKm:      0,
			wantAzimuth: 0,
			wantErr:     false,
		},

		// ///////////////////////
		//  https://www.movable-type.co.uk/scripts/latlong.html
		{
			name: "Point-1 to Point-2",
			args: args{
				latLonDmsA: location.LatLonDMS{
					Latitude: location.DMS{
						Degrees: 50,
						Minutes: 3,
						Seconds: 59,
					},
					Longitude: location.DMS{
						Degrees: -5,
						Minutes: 42,
						Seconds: 53,
					},
				},
				latLonDmsB: location.LatLonDMS{
					Latitude: location.DMS{
						Degrees: 58,
						Minutes: 38,
						Seconds: 38,
					},
					Longitude: location.DMS{
						Degrees: -3,
						Minutes: 4,
						Seconds: 12,
					},
				},
			},
			wantKm:      968.8548823543566,
			wantAzimuth: 9.119817327410368,
			wantErr:     false,
		},
		{
			name: "Point-2 to Point-1",
			args: args{
				latLonDmsA: location.LatLonDMS{
					Latitude: location.DMS{
						Degrees: 58,
						Minutes: 38,
						Seconds: 38,
					},
					Longitude: location.DMS{
						Degrees: -3,
						Minutes: 4,
						Seconds: 12,
					},
				},
				latLonDmsB: location.LatLonDMS{
					Latitude: location.DMS{
						Degrees: 50,
						Minutes: 3,
						Seconds: 59,
					},
					Longitude: location.DMS{
						Degrees: -5,
						Minutes: 42,
						Seconds: 53,
					},
				},
			},
			wantKm:      968.8548823543566,
			wantAzimuth: 191.27520031620156,
			wantErr:     false,
		},

		// //////////////////////
		// https://edwilliams.org/avform147.htm#Intro
		{
			name: "LAX to JFK",
			args: args{
				latLonDmsA: location.LatLonDMS{
					Latitude: location.DMS{
						Degrees: 33,
						Minutes: 57,
						Seconds: 0,
					},
					Longitude: location.DMS{
						Degrees: -118,
						Minutes: 24,
						Seconds: 0,
					},
				},
				latLonDmsB: location.LatLonDMS{
					Latitude: location.DMS{
						Degrees: 40,
						Minutes: 37,
						Seconds: 59,
					},
					Longitude: location.DMS{
						Degrees: -73,
						Minutes: 46,
						Seconds: 59,
					},
				},
			},
			wantKm:      3972.888729493,
			wantAzimuth: 65.8921670931295,
			wantErr:     false,
		},
		{
			name: "JFK to LAX",
			args: args{
				latLonDmsA: location.LatLonDMS{
					Latitude: location.DMS{
						Degrees: 40,
						Minutes: 37,
						Seconds: 59,
					},
					Longitude: location.DMS{
						Degrees: -73,
						Minutes: 46,
						Seconds: 59,
					},
				},
				latLonDmsB: location.LatLonDMS{
					Latitude: location.DMS{
						Degrees: 33,
						Minutes: 57,
						Seconds: 0,
					},
					Longitude: location.DMS{
						Degrees: -118,
						Minutes: 24,
						Seconds: 0,
					},
				},
			},
			wantKm:      3972.888729493,
			wantAzimuth: 273.8581644724343,
			wantErr:     false,
		},
		{
			name: "invalid latitude",
			args: args{
				latLonDmsA: location.LatLonDMS{
					Latitude: location.DMS{
						Degrees: 0,
						Minutes: -60,
						Seconds: 0,
					},
					Longitude: location.DMS{
						Degrees: 0,
						Minutes: 0,
						Seconds: 0,
					},
				},
				latLonDmsB: location.LatLonDMS{
					Latitude: location.DMS{
						Degrees: 0,
						Minutes: 0,
						Seconds: 0,
					},
					Longitude: location.DMS{
						Degrees: 0,
						Minutes: 0,
						Seconds: 0,
					},
				},
			},
			wantKm:      0,
			wantAzimuth: 0,
			wantErr:     true,
		},
		{
			name: "invalid longitude",
			args: args{
				latLonDmsA: location.LatLonDMS{
					Latitude: location.DMS{
						Degrees: 0,
						Minutes: 0,
						Seconds: 0,
					},
					Longitude: location.DMS{
						Degrees: 0,
						Minutes: 0,
						Seconds: 0,
					},
				},
				latLonDmsB: location.LatLonDMS{
					Latitude: location.DMS{
						Degrees: 0,
						Minutes: 0,
						Seconds: 0,
					},
					Longitude: location.DMS{
						Degrees: 0,
						Minutes: 0,
						Seconds: -60,
					},
				},
			},
			wantKm:      0,
			wantAzimuth: 0,
			wantErr:     true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotKm, gotAzimuth, err := BetweenLatLonDMS(tt.args.latLonDmsA, tt.args.latLonDmsB)
			if (err != nil) != tt.wantErr {
				t.Errorf("BetweenLatLonDMS() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !geoloc.AlmostEqual(gotKm, tt.wantKm) {
				t.Errorf("BetweenLatLonDMS() gotKm = %v, want %v", gotKm, tt.wantKm)
			}
			if !geoloc.AlmostEqual(gotAzimuth, tt.wantAzimuth) {
				t.Errorf("BetweenLatLonDMS() gotAzimuth = %v, want %v", gotAzimuth, tt.wantAzimuth)
			}
		})
	}
}
