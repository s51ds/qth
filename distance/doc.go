// Package distance calculates the distance between two positions on Earth.
// Positions can be specified as latitude/longitude or Maidenhead QTH locators.
// The location package handles conversions between these formats.
package distance
