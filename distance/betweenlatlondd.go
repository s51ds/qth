package distance

import (
	"github.com/golang/geo/s2"
	"gitlab.com/s51ds/qth/location"
)

// BetweenLatLonDD calculates the distance in kilometers and azimuth in decimal degrees between two LatLonDD points.
// Takes two location.LatLonDD coordinates. Returns distance in km, azimuth, and error if coordinates are invalid.
func BetweenLatLonDD(latLonA, latLonB location.LatLonDD) (km, azimuth float64, err error) {
	if err = latLonA.Valid(); err != nil {
		return 0, 0, err
	}
	if err = latLonB.Valid(); err != nil {
		return 0, 0, err
	}
	latLngA := s2.LatLngFromDegrees(latLonA.Latitude, latLonA.Longitude)
	latLngB := s2.LatLngFromDegrees(latLonB.Latitude, latLonB.Longitude)
	a := latLngA.Distance(latLngB)
	km = a.Radians() * earthRadiusKm

	azimuth = azimuthTo(latLngA, latLngB)

	return km, azimuth, nil
}

// BetweenLatLonDDkm returns the distance in kilometers between two LatLonDD points.
// Coordinates must be valid (Latitude between -90 and 90, Longitude between -180 and 180).
func BetweenLatLonDDkm(latLonA, latLonB location.LatLonDD) float64 {
	latLngA := s2.LatLngFromDegrees(latLonA.Latitude, latLonA.Longitude)
	latLngB := s2.LatLngFromDegrees(latLonB.Latitude, latLonB.Longitude)
	return latLngA.Distance(latLngB).Radians() * earthRadiusKm
}
