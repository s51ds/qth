package distance

import (
	"fmt"
	"testing"

	"gitlab.com/s51ds/qth/location"
)

func TestExample(t *testing.T) {

	// https://edwilliams.org/avform147.htm#Intro
	//  Suppose point 1 is LAX: (33deg 57min N, 118deg 24min W)
	//  Suppose point 2 is JFK: (40deg 38min N,  73deg 47min W)
	LAX := location.LatLonDMS{
		Latitude: location.DMS{
			Degrees: 33,
			Minutes: 57,
		},
		Longitude: location.DMS{
			Degrees: -118,
			Minutes: 24,
		},
	}
	JFK := location.LatLonDMS{
		Latitude: location.DMS{
			Degrees: 40,
			Minutes: 38,
		},
		Longitude: location.DMS{
			Degrees: -73,
			Minutes: 47,
		},
	}

	// The distance from LAX to JFK is 2144nm
	// The initial true course out of LAX is 66 deg
	dist, azim, _ := BetweenLatLonDMS(LAX, JFK)
	fmt.Println(dist, azim)

	qthJFK, _ := location.ConvertLatLonDMStoLocator(JFK)
	qthLAX, _ := location.ConvertLatLonDMStoLocator(LAX)
	fmt.Println(qthJFK, qthLAX)
	dist, azim, _ = BetweenQth(qthLAX, qthJFK)
	fmt.Println(dist, azim)

}
