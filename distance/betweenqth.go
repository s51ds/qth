package distance

import (
	"gitlab.com/s51ds/qth/location"
)

// BetweenQth calculates the distance in kilometers and azimuth between two QTH locators.
// Takes two locator strings and returns the distance in km, azimuth in decimal degrees, and error if input is invalid.
func BetweenQth(locatorA, locatorB string) (km, azimuth float64, err error) {

	var latLonA, latLonB location.LatLonDD

	latLonA, err = location.ConvertLocatorToLatLonDD(locatorA)
	if err != nil {
		return 0, 0, err
	}
	latLonB, err = location.ConvertLocatorToLatLonDD(locatorB)
	if err != nil {
		return 0, 0, err
	}

	return BetweenLatLonDD(latLonA, latLonB)
}
