package distance

import (
	"testing"

	"gitlab.com/s51ds/qth/internal/geoloc"
)

func TestDistance(t *testing.T) {
	type args struct {
		locatorA string
		locatorB string
	}
	tests := []struct {
		name        string
		args        args
		wantKm      float64
		wantAzimuth float64
		wantErr     bool
	}{
		{
			name: "s59abc-s59abc",
			args: args{
				locatorA: "JN76TO",
				locatorB: "JN76TO",
			},
			wantKm:      0,
			wantAzimuth: 0,
			wantErr:     false,
		},
		{
			name: "s59abc-s57m",
			args: args{
				locatorA: "JN76TO",
				locatorB: "JN76PO",
			},
			wantKm:      25.46,
			wantAzimuth: 270.1211,
			wantErr:     false,
		},
		{
			name: "s57m-s59abc",
			args: args{
				locatorA: "JN76PO",
				locatorB: "JN76TO",
			},
			wantKm:      25.46,
			wantAzimuth: 89.8788,
			wantErr:     false,
		},
		{
			name: "North-South-Pole",
			args: args{
				locatorA: "AR09AX",
				locatorB: "AA00AA",
			},
			wantKm:      20010.481313695705,
			wantAzimuth: 180,
			wantErr:     false,
		},
		{
			name: "South-North-Pole",
			args: args{
				locatorA: "AA00AA",
				locatorB: "AR09AX",
			},
			wantKm:      20010.481313695705,
			wantAzimuth: 0,
			wantErr:     false,
		},
		{
			name: "ZRS Septembrsko UKV Tekmovanje 2023 ODX",
			args: args{
				locatorA: "JN76JG",
				locatorB: "IO70JC",
			},
			wantKm:      1537.42, // 1538
			wantAzimuth: 293.37,
			wantErr:     false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotKm, gotAzimuth, err := BetweenQth(tt.args.locatorA, tt.args.locatorB)
			if !geoloc.AlmostEqual(gotKm, tt.wantKm) {
				t.Errorf("BetweenQth() gotKm = %v, want %v", gotKm, tt.wantKm)
			}
			if !geoloc.AlmostEqual(gotAzimuth, tt.wantAzimuth) {
				t.Errorf("BetweenQth() gotAzimuth = %v, want %v", gotAzimuth, tt.wantAzimuth)
			}
			if (err != nil) != tt.wantErr {
				t.Errorf("BetweenQth() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
