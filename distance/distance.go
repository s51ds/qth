package distance

import (
	"math"

	"github.com/golang/geo/s2"
)

const (
	earthRadiusKm = 6371.0087714 // mean Earth radius in km
	rad           = 180.0 / math.Pi
)

// azimuthTo Calculates forward azimuth in decimal degrees from a to b
// Original Implementation from: http://www.movable-type.co.uk/scripts/latlong.html
func azimuthTo(a, b s2.LatLng) float64 {
	diffInLongitude := (b.Lng - a.Lng).Radians()

	lat1 := a.Lat.Radians()
	lat2 := b.Lat.Radians()

	y := math.Sin(diffInLongitude) * math.Cos(lat2)
	x := math.Cos(lat1)*math.Sin(lat2) - math.Sin(lat1)*math.Cos(lat2)*math.Cos(diffInLongitude)

	azimDeg := math.Atan2(y, x) * rad
	if azimDeg < 0 {
		return 360.0 + azimDeg
	} else {
		return azimDeg
	}
}
