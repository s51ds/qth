package location

import (
	"errors"
	"fmt"
	"math"
	"regexp"
	"strings"

	"gitlab.com/s51ds/qth/internal/geoloc"
)

// ConvertLatLonDDtoLocator converts a given latitude and longitude in decimal degrees
// to a Maidenhead grid locator string. Returns an error if the input coordinates are invalid.
func ConvertLatLonDDtoLocator(latLonDD LatLonDD) (locator string, err error) {
	if err = latLonDD.Valid(); err != nil {
		return "", err
	}

	lld := geoloc.LatLonDeg{Latitude: latLonDD.Latitude, Longitude: latLonDD.Longitude}
	field, square, subsquare := geoloc.SubsquareEncode(lld)
	locator = field.Encoded.GetLonChar() + field.Encoded.GetLatChar() + square.Encoded.GetLonChar() + square.Encoded.GetLatChar() + subsquare.Encoded.GetLonChar() + subsquare.Encoded.GetLatChar()
	return locator, nil
}

// ConvertLatLonDMStoLocator converts a given latitude and longitude in DMS
// to a Maidenhead grid locator string. Returns an error if the input coordinates are invalid.
func ConvertLatLonDMStoLocator(latLonDMS LatLonDMS) (locator string, err error) {
	if err = latLonDMS.Valid(); err != nil {
		return "", err
	}
	latLonDD, err := ConvertLatLonDMStoLatLonDD(latLonDMS)
	if err != nil {
		return "", err
	}

	lld := geoloc.LatLonDeg{Latitude: latLonDD.Latitude, Longitude: latLonDD.Longitude}
	field, square, subsquare := geoloc.SubsquareEncode(lld)
	locator = field.Encoded.GetLonChar() + field.Encoded.GetLatChar() + square.Encoded.GetLonChar() + square.Encoded.GetLatChar() + subsquare.Encoded.GetLonChar() + subsquare.Encoded.GetLatChar()
	return locator, nil
}

var (
	reX6 = regexp.MustCompile(`^[A-R]{2}[0-9]{2}[A-X]{2}$`)
	reX4 = regexp.MustCompile(`^[A-R]{2}[0-9]{2}$`)
	reX2 = regexp.MustCompile(`^[A-R]{2}$`)
)

// ConvertLocatorToLatLonDD returns the latitude and longitude in decimal degrees for a given QTH locator.
// The locator is case-insensitive and can be 2, 4, or 6 characters long.
// Examples of valid locators include:
// - "JN"
// - "jN76"
// - "jn76TO"
// If the locator is invalid, an error is returned.
func ConvertLocatorToLatLonDD(locator string) (latLonDD LatLonDD, err error) {
	locator = strings.ToUpper(locator)
	switch len(locator) {
	case 6:
		{
			if matched := reX6.MatchString(locator); matched {
				f := geoloc.FieldDecode(geoloc.LatLonChar{
					LatChar: locator[1],
					LonChar: locator[0],
				})
				s := geoloc.SquareDecode(geoloc.LatLonChar{
					LatChar: locator[3],
					LonChar: locator[2],
				})
				ss := geoloc.SubsquareDecode(geoloc.LatLonChar{
					LatChar: locator[5],
					LonChar: locator[4],
				})
				latLonDD.Latitude = f.Decoded.Latitude + s.Decoded.Latitude + ss.Decoded.Latitude/60 + 0.02083333     // 1.25' / 60
				latLonDD.Longitude = f.Decoded.Longitude + s.Decoded.Longitude + ss.Decoded.Longitude/60 + 0.04166667 // 2.5' / 60
				return latLonDD, nil

			}

		}
	case 4:
		{
			if matched := reX4.MatchString(locator); matched {
				f := geoloc.FieldDecode(geoloc.LatLonChar{
					LatChar: locator[1],
					LonChar: locator[0],
				})
				s := geoloc.SquareDecode(geoloc.LatLonChar{
					LatChar: locator[3],
					LonChar: locator[2],
				})
				latLonDD.Latitude = f.Decoded.Latitude + s.Decoded.Latitude + 0.5
				latLonDD.Longitude = f.Decoded.Longitude + s.Decoded.Longitude + 1
				return latLonDD, nil
			}

		}
	case 2:
		{
			if matched := reX2.MatchString(locator); matched {
				f := geoloc.FieldDecode(geoloc.LatLonChar{
					LatChar: locator[1],
					LonChar: locator[0],
				})
				latLonDD.Latitude = f.Decoded.Latitude + 5
				latLonDD.Longitude = f.Decoded.Longitude + 10
				return latLonDD, nil
			}
		}

	default:
		return latLonDD, errors.New(fmt.Sprintf("Illegal argumet value! qthLocator=%s", locator))
	}
	return latLonDD, errors.New(fmt.Sprintf("Illegal argumet value! qthLocator=%s", locator))
}

// ConvertLocatorToLatLonDMS returns the latitude and longitude in DMS format for a given QTH locator.
// The locator is case-insensitive and can be 2, 4, or 6 characters long.
// Examples of valid locators include:
// - "JN"
// - "jN76"
// - "jn76TO"
// If the locator is invalid, an error is returned.
func ConvertLocatorToLatLonDMS(locator string) (latLonDMS LatLonDMS, err error) {
	latLonDD, err := ConvertLocatorToLatLonDD(locator)
	if err != nil {
		return latLonDMS, err
	}
	latLonDMS = ConvertLatLonDDToLatLonDMS(latLonDD)
	return latLonDMS, nil
}

// ConvertDMStoDD converts a DMS value to decimal degrees.
func ConvertDMStoDD(dms DMS) float64 {
	negativeValue := dms.Degrees < 0
	decimal := math.Abs(float64(dms.Degrees)) + float64(dms.Minutes)/60 + float64(dms.Seconds)/3600
	if negativeValue {
		return -decimal
	}
	return decimal
}

// ConvertDDtoDMS converts a decimal degree value to degrees, minutes, and seconds (DMS) format.
// The seconds are rounded to the nearest whole number.
func ConvertDDtoDMS(decimalDegree float64) DMS {
	absDecimalDegree := math.Abs(decimalDegree)
	degrees := int(absDecimalDegree)
	remaining := (absDecimalDegree - float64(degrees)) * 60
	minutes := int(remaining)
	seconds := math.Round((remaining - float64(minutes)) * 60)

	if seconds >= 60 {
		seconds = 0
		minutes++
		if minutes == 60 {
			minutes = 0
			degrees++
		}
	}

	if decimalDegree < 0 {
		degrees = -degrees
	}

	return DMS{
		Degrees: degrees,
		Minutes: minutes,
		Seconds: seconds,
	}
}

// ConvertLatLonDMStoLatLonDD converts latitude and longitude from DMS (degrees, minutes, seconds)
// format to DD (decimal degrees) format. It returns an error if the input is invalid.
func ConvertLatLonDMStoLatLonDD(latLonDMS LatLonDMS) (latLonDD LatLonDD, err error) {
	if err = latLonDMS.Valid(); err != nil {
		return latLonDD, err
	}
	latLonDD.Longitude = ConvertDMStoDD(latLonDMS.Longitude)
	latLonDD.Latitude = ConvertDMStoDD(latLonDMS.Latitude)
	return latLonDD, nil
}

// ConvertLatLonDDToLatLonDMS converts latitude and longitude from DD (decimal degrees)
// format to DMS (degrees, minutes, seconds) format.
func ConvertLatLonDDToLatLonDMS(a LatLonDD) LatLonDMS {
	return LatLonDMS{
		Latitude:  ConvertDDtoDMS(a.Latitude),
		Longitude: ConvertDDtoDMS(a.Longitude),
	}
}
