package location

import (
	"fmt"
	"strconv"
)

// LatLonDD represents geographical coordinates in decimal degrees.
// Latitude: Positive values indicate North, negative values indicate South, ranging from -90 to 90
// Longitude: Positive values indicate East, negative values indicate West, -180 to 180
type LatLonDD struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

// Valid checks if the LatLonDD instance has valid latitude and longitude values as per the allowed ranges.
func (a LatLonDD) Valid() error {
	if a.Latitude < -90 || a.Latitude > 90 {
		return fmt.Errorf("latitude must be between -90 and 90, was %f", a.Latitude)
	}
	if a.Longitude < -180 || a.Longitude > 180 {
		return fmt.Errorf("longitude must be between -180 and 180, was %f", a.Longitude)
	}
	return nil
}

// String returns the LatLonDD coordinates as a formatted string
// with latitude and longitude in decimal  degrees, followed by their respective
// hemispheres (N/S for latitude, E/W for longitude).
func (a LatLonDD) String() string {
	var NS, EW string
	lat := a.Latitude
	lon := a.Longitude

	if lat < 0 {
		NS = "S"
		lat = -lat
	} else {
		NS = "N"
	}
	if lon < 0 {
		EW = "W"
		lon = -lon
	} else {
		EW = "E"
	}

	return strconv.FormatFloat(lat, 'f', 6, 64) + NS + " " + strconv.FormatFloat(lon, 'f', 6, 64) + EW
}

// String2dec returns the LatLonDD coordinates as a formatted string
// with latitude and longitude in decimal  degrees to two decimal places,
// followed by their respective hemispheres (N/S for latitude, E/W for longitude).
func (a LatLonDD) String2dec() string {
	var NS, EW string
	lat := a.Latitude
	lon := a.Longitude

	if lat < 0 {
		NS = "S"
		lat = -lat
	} else {
		NS = "N"
	}
	if lon < 0 {
		EW = "W"
		lon = -lon
	} else {
		EW = "E"
	}

	return strconv.FormatFloat(lat, 'f', 2, 64) + NS + " " + strconv.FormatFloat(lon, 'f', 2, 64) + EW
}

// Equals compares two LatLonDD instances and returns true if they have the same string representation.
func (a LatLonDD) Equals(b LatLonDD) bool {
	return a.String() == b.String()
}
