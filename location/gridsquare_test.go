package location

import (
	"testing"
)

func TestGridSquare_String(t *testing.T) {
	gridSquareTests := []struct {
		name string
		gs   GridSquare
		want string
	}{
		{
			name: "JN",
			gs: GridSquare{
				Name: "JN",
				A: LatLonDD{
					Latitude:  40,
					Longitude: 0,
				},
				B: LatLonDD{
					Latitude:  50,
					Longitude: 0,
				},
				C: LatLonDD{
					Latitude:  50,
					Longitude: 20,
				},
				D: LatLonDD{
					Latitude:  40,
					Longitude: 20,
				},
				X: LatLonDD{
					Latitude:  45,
					Longitude: 10,
				},
			},
			want: `JN [45.000000N 10.000000E]
B=[50.000000N 0.000000E], C=[50.000000N 20.000000E]
A=[40.000000N 0.000000E], D=[40.000000N 20.000000E]
`,
		},
	}

	for _, tt := range gridSquareTests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.gs.String(); got != tt.want {
				t.Errorf("GridSquare.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGridSquare_Equals(t *testing.T) {
	type fields struct {
		Name string
		A    LatLonDD
		B    LatLonDD
		C    LatLonDD
		D    LatLonDD
		X    LatLonDD
	}
	type args struct {
		b GridSquare
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "zero",
			fields: fields{
				Name: "",
				A: LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				B: LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				C: LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				D: LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				X: LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
			},
			args: args{
				b: GridSquare{
					Name: "",
					A: LatLonDD{
						Latitude:  0,
						Longitude: 0,
					},
					B: LatLonDD{
						Latitude:  0,
						Longitude: 0,
					},
					C: LatLonDD{
						Latitude:  0,
						Longitude: 0,
					},
					D: LatLonDD{
						Latitude:  0,
						Longitude: 0,
					},
					X: LatLonDD{
						Latitude:  0,
						Longitude: 0,
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := GridSquare{
				Name: tt.fields.Name,
				A:    tt.fields.A,
				B:    tt.fields.B,
				C:    tt.fields.C,
				D:    tt.fields.D,
				X:    tt.fields.X,
			}
			if got := a.Equals(tt.args.b); got != tt.want {
				t.Errorf("Equals() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetLocatorGridSquare(t *testing.T) {
	type args struct {
		locator string
	}
	tests := []struct {
		name    string
		args    args
		wantGs  GridSquare
		wantErr bool
	}{
		// ///////
		// two characters
		{
			name: "JJ",
			args: args{
				locator: "JJ",
			},
			wantGs: GridSquare{
				Name: "JJ",
				A: LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				B: LatLonDD{
					Latitude:  10,
					Longitude: 0,
				},
				C: LatLonDD{
					Latitude:  10,
					Longitude: 20,
				},
				D: LatLonDD{
					Latitude:  0,
					Longitude: 20,
				},
				X: LatLonDD{
					Latitude:  5,
					Longitude: 10,
				},
			},
			wantErr: false,
		},
		{ // up from JJ
			name: "JK",
			args: args{
				locator: "JK",
			},
			wantGs: GridSquare{
				Name: "JK",
				A: LatLonDD{
					Latitude:  10,
					Longitude: 0,
				},
				B: LatLonDD{
					Latitude:  20,
					Longitude: 0,
				},
				C: LatLonDD{
					Latitude:  20,
					Longitude: 20,
				},
				D: LatLonDD{
					Latitude:  10,
					Longitude: 20,
				},
				X: LatLonDD{
					Latitude:  15,
					Longitude: 10,
				},
			},
			wantErr: false,
		},
		{ // down from JJ
			name: "JI",
			args: args{
				locator: "JI",
			},
			wantGs: GridSquare{
				Name: "JI",
				A: LatLonDD{
					Latitude:  -10,
					Longitude: 0,
				},
				B: LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				C: LatLonDD{
					Latitude:  0,
					Longitude: 20,
				},
				D: LatLonDD{
					Latitude:  -10,
					Longitude: 20,
				},
				X: LatLonDD{
					Latitude:  -5,
					Longitude: 10,
				},
			},
			wantErr: false,
		},
		{ // right from JJ
			name: "KJ",
			args: args{
				locator: "KJ",
			},
			wantGs: GridSquare{
				Name: "KJ",
				A: LatLonDD{
					Latitude:  0,
					Longitude: 20,
				},
				B: LatLonDD{
					Latitude:  10,
					Longitude: 20,
				},
				C: LatLonDD{
					Latitude:  10,
					Longitude: 40,
				},
				D: LatLonDD{
					Latitude:  0,
					Longitude: 40,
				},
				X: LatLonDD{
					Latitude:  5,
					Longitude: 30,
				},
			},
			wantErr: false,
		},
		{ // left from JJ
			name: "IJ",
			args: args{
				locator: "IJ",
			},
			wantGs: GridSquare{
				Name: "IJ",
				A: LatLonDD{
					Latitude:  0,
					Longitude: -20,
				},
				B: LatLonDD{
					Latitude:  10,
					Longitude: -20,
				},
				C: LatLonDD{
					Latitude:  10,
					Longitude: 0,
				},
				D: LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				X: LatLonDD{
					Latitude:  5,
					Longitude: -10,
				},
			},
			wantErr: false,
		},

		// ///////
		// four characters
		{
			name: "JJ00",
			args: args{
				locator: "JJ00",
			},
			wantGs: GridSquare{
				Name: "JJ00",
				A: LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				B: LatLonDD{
					Latitude:  1,
					Longitude: 0,
				},
				C: LatLonDD{
					Latitude:  1,
					Longitude: 2,
				},
				D: LatLonDD{
					Latitude:  0,
					Longitude: 2,
				},
				X: LatLonDD{
					Latitude:  0.5,
					Longitude: 1,
				},
			},
			wantErr: false,
		},
		{ // up from JJ00
			name: "JJ01",
			args: args{
				locator: "JJ01",
			},
			wantGs: GridSquare{
				Name: "JJ01",
				A: LatLonDD{
					Latitude:  1,
					Longitude: 0,
				},
				B: LatLonDD{
					Latitude:  2,
					Longitude: 0,
				},
				C: LatLonDD{
					Latitude:  2,
					Longitude: 2,
				},
				D: LatLonDD{
					Latitude:  1,
					Longitude: 2,
				},
				X: LatLonDD{
					Latitude:  1.5,
					Longitude: 1,
				},
			},
			wantErr: false,
		},
		{ // down from JJ00
			name: "JI09",
			args: args{
				locator: "JI09",
			},
			wantGs: GridSquare{
				Name: "JI09",
				A: LatLonDD{
					Latitude:  -1,
					Longitude: 0,
				},
				B: LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				C: LatLonDD{
					Latitude:  0,
					Longitude: 2,
				},
				D: LatLonDD{
					Latitude:  -1,
					Longitude: 2,
				},
				X: LatLonDD{
					Latitude:  -0.5,
					Longitude: 1,
				},
			},
			wantErr: false,
		},
		{ // right from JJ00
			name: "JJ10",
			args: args{
				locator: "JJ10",
			},
			wantGs: GridSquare{
				Name: "JJ10",
				A: LatLonDD{
					Latitude:  0,
					Longitude: 2,
				},
				B: LatLonDD{
					Latitude:  1,
					Longitude: 2,
				},
				C: LatLonDD{
					Latitude:  1,
					Longitude: 4,
				},
				D: LatLonDD{
					Latitude:  0,
					Longitude: 4,
				},
				X: LatLonDD{
					Latitude:  0.5,
					Longitude: 3,
				},
			},
			wantErr: false,
		},
		{ // left from JJ00
			name: "IJ90",
			args: args{
				locator: "IJ90",
			},
			wantGs: GridSquare{
				Name: "IJ90",
				A: LatLonDD{
					Latitude:  0,
					Longitude: -2,
				},
				B: LatLonDD{
					Latitude:  1,
					Longitude: -2,
				},
				C: LatLonDD{
					Latitude:  1,
					Longitude: 0,
				},
				D: LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				X: LatLonDD{
					Latitude:  0.5,
					Longitude: -1,
				},
			},
			wantErr: false,
		},

		// ///////
		// six characters
		{
			name: "JJ00AA",
			args: args{
				locator: "JJ00AA",
			},
			wantGs: GridSquare{
				Name: "JJ00AA",
				A: LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
				B: LatLonDD{
					Latitude:  0.041667,
					Longitude: 0,
				},
				C: LatLonDD{
					Latitude:  0.041667,
					Longitude: 0.083333,
				},
				D: LatLonDD{
					Latitude:  0,
					Longitude: 0.083333,
				},
				X: LatLonDD{
					Latitude:  0.020833,
					Longitude: 0.041667,
				},
			},
			wantErr: false,
		},

		{
			name: "JN",
			args: args{
				locator: "JN",
			},
			wantGs: GridSquare{
				Name: "JN",
				A: LatLonDD{
					Latitude:  40,
					Longitude: 0,
				},
				B: LatLonDD{
					Latitude:  50,
					Longitude: 0,
				},
				C: LatLonDD{
					Latitude:  50,
					Longitude: 20,
				},
				D: LatLonDD{
					Latitude:  40,
					Longitude: 20,
				},
				X: LatLonDD{
					Latitude:  45,
					Longitude: 10,
				},
			},
			wantErr: false,
		},
		{
			name: "JN76",
			args: args{
				locator: "JN76",
			},
			wantGs: GridSquare{
				Name: "JN76",
				A: LatLonDD{
					Latitude:  46,
					Longitude: 14,
				},
				B: LatLonDD{
					Latitude:  47,
					Longitude: 14,
				},
				C: LatLonDD{
					Latitude:  47,
					Longitude: 16,
				},
				D: LatLonDD{
					Latitude:  46,
					Longitude: 16,
				},
				X: LatLonDD{
					Latitude:  46.5,
					Longitude: 15,
				},
			},
			wantErr: false,
		},
		{
			name: "JN76TO",
			args: args{
				locator: "JN76TO",
			},
			wantGs: GridSquare{
				Name: "JN76TO",
				A: LatLonDD{
					Latitude:  46.583333,
					Longitude: 15.583333,
				},
				B: LatLonDD{
					Latitude:  46.625000,
					Longitude: 15.583333,
				},
				C: LatLonDD{
					Latitude:  46.625000,
					Longitude: 15.666667,
				},
				D: LatLonDD{
					Latitude:  46.583333,
					Longitude: 15.666667,
				},
				X: LatLonDD{
					Latitude:  46.604167,
					Longitude: 15.625000,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotGs, err := GetLocatorGridSquare(tt.args.locator)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetLocatorGridSquare() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !gotGs.Equals(tt.wantGs) {
				t.Errorf("GetLocatorGridSquare() \ngotGs = %s, \nwant %s", gotGs.String(), tt.wantGs.String())
			}
		})
	}
}
