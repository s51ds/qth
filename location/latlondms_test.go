package location

import "testing"

func TestDMS_String(t *testing.T) {
	type fields struct {
		Degrees int
		Minutes int
		Seconds float64
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "0",
			fields: fields{
				Degrees: 0,
				Minutes: 0,
				Seconds: 0,
			},
			want: `0°0'0.00"`,
		},
		{
			name: "S59ABC-latitude",
			fields: fields{
				Degrees: 46,
				Minutes: 36,
				Seconds: 14,
			},
			want: `46°36'14.00"`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &DMS{
				Degrees: tt.fields.Degrees,
				Minutes: tt.fields.Minutes,
				Seconds: tt.fields.Seconds,
			}
			if got := a.String(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLatLonDMS_ToLatLon(t *testing.T) {
	type fields struct {
		Latitude  DMS
		Longitude DMS
	}
	tests := []struct {
		name   string
		fields fields
		want   LatLonDD
	}{
		{
			name: "0",
			fields: fields{
				Latitude: DMS{
					Degrees: 0,
					Minutes: 0,
					Seconds: 0,
				},
				Longitude: DMS{
					Degrees: 0,
					Minutes: 0,
					Seconds: 0,
				},
			},
			want: LatLonDD{
				Latitude:  0,
				Longitude: 0,
			},
		},
		{
			name: "S59ABC",
			fields: fields{
				Latitude: DMS{
					Degrees: 46,
					Minutes: 36,
					Seconds: 14,
				},
				Longitude: DMS{
					Degrees: 15,
					Minutes: 37,
					Seconds: 30,
				},
			},
			want: LatLonDD{
				Latitude:  46.603889,
				Longitude: 15.625000003333334,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &LatLonDMS{
				Latitude:  tt.fields.Latitude,
				Longitude: tt.fields.Longitude,
			}
			got, err := ConvertLatLonDMStoLatLonDD(*a)
			if err != nil {
				t.Errorf("ConvertLatLonDMStoLatLonDD() error = %v", err)
			}
			if got.String() != tt.want.String() {
				t.Errorf("LatLonDD() = %v, want %v", got.String(), tt.want.String())
			}
		})
	}
}

func TestLatLonDMS_String(t *testing.T) {
	type fields struct {
		Latitude  DMS
		Longitude DMS
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "0",
			fields: fields{
				Latitude: DMS{
					Degrees: 0,
					Minutes: 0,
					Seconds: 0,
				},
				Longitude: DMS{
					Degrees: 0,
					Minutes: 0,
					Seconds: 0,
				},
			},
			want: `0°0'0.00"N 0°0'0.00"E`,
		},
		{
			name: "S59ABC",
			fields: fields{
				Latitude: DMS{
					Degrees: 46,
					Minutes: 36,
					Seconds: 14,
				},
				Longitude: DMS{
					Degrees: 15,
					Minutes: 37,
					Seconds: 30,
				},
			},
			want: `46°36'14.00"N 15°37'30.00"E`,
		},
		{
			name: "SW",
			fields: fields{
				Latitude: DMS{
					Degrees: -46,
					Minutes: 36,
					Seconds: 14,
				},
				Longitude: DMS{
					Degrees: -15,
					Minutes: 37,
					Seconds: 30,
				},
			},
			want: `46°36'14.00"S 15°37'30.00"W`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &LatLonDMS{
				Latitude:  tt.fields.Latitude,
				Longitude: tt.fields.Longitude,
			}
			if got := a.String(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}
func TestDMS_Valid(t *testing.T) {
	type fields struct {
		Degrees int
		Minutes int
		Seconds float64
	}
	tests := []struct {
		name    string
		fields  fields
		isLat   bool
		wantErr bool
	}{
		{
			name: "valid-lat",
			fields: fields{
				Degrees: 46,
				Minutes: 36,
				Seconds: 14,
			},
			isLat:   true,
			wantErr: false,
		},
		{
			name: "valid-long",
			fields: fields{
				Degrees: 15,
				Minutes: 37,
				Seconds: 30,
			},
			isLat:   false,
			wantErr: false,
		},
		{
			name: "-valid-lat",
			fields: fields{
				Degrees: -46,
				Minutes: 36,
				Seconds: 14,
			},
			isLat:   true,
			wantErr: false,
		},
		{
			name: "-valid-long",
			fields: fields{
				Degrees: -15,
				Minutes: 37,
				Seconds: 30,
			},
			isLat:   false,
			wantErr: false,
		},

		{
			name: "invalid-lat",
			fields: fields{
				Degrees: 91,
				Minutes: 60,
				Seconds: 60,
			},
			isLat:   true,
			wantErr: true,
		},
		{
			name: "invalid-long",
			fields: fields{
				Degrees: 181,
				Minutes: 0,
				Seconds: 0,
			},
			isLat:   false,
			wantErr: true,
		},
		{
			name: "-invalid-lat",
			fields: fields{
				Degrees: -91,
				Minutes: 60,
				Seconds: 60,
			},
			isLat:   true,
			wantErr: true,
		},
		{
			name: "-invalid-long",
			fields: fields{
				Degrees: -181,
				Minutes: 0,
				Seconds: 0,
			},
			isLat:   false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &DMS{
				Degrees: tt.fields.Degrees,
				Minutes: tt.fields.Minutes,
				Seconds: tt.fields.Seconds,
			}
			err := a.Valid(tt.isLat)
			if (err != nil) != tt.wantErr {
				t.Errorf("Valid() error = %v, wantErr = %v", err, tt.wantErr)
			}
		})
	}
}

func TestLatLonDMS_Valid(t *testing.T) {
	tests := []struct {
		name    string
		latDMS  DMS
		longDMS DMS
		wantErr bool
	}{
		{
			name: "valid",
			latDMS: DMS{
				Degrees: 46,
				Minutes: 36,
				Seconds: 14,
			},
			longDMS: DMS{
				Degrees: 15,
				Minutes: 37,
				Seconds: 30,
			},
			wantErr: false,
		},
		{
			name: "valid-1",
			latDMS: DMS{
				Degrees: -46,
				Minutes: 36,
				Seconds: 14,
			},
			longDMS: DMS{
				Degrees: -15,
				Minutes: 37,
				Seconds: 30,
			},
			wantErr: false,
		},
		{
			name: "invalid-lat-1",
			latDMS: DMS{
				Degrees: 91,
				Minutes: 0,
				Seconds: 0,
			},
			longDMS: DMS{
				Degrees: 15,
				Minutes: 30,
				Seconds: 0,
			},
			wantErr: true,
		},
		{
			name: "invalid-long-1",
			latDMS: DMS{
				Degrees: 46,
				Minutes: 36,
				Seconds: 14,
			},
			longDMS: DMS{
				Degrees: 181,
				Minutes: 0,
				Seconds: 0,
			},
			wantErr: true,
		},
		{
			name: "invalid-lat-2",
			latDMS: DMS{
				Degrees: -91,
				Minutes: 0,
				Seconds: 0,
			},
			longDMS: DMS{
				Degrees: 15,
				Minutes: 30,
				Seconds: 0,
			},
			wantErr: true,
		},
		{
			name: "invalid-long-2",
			latDMS: DMS{
				Degrees: 46,
				Minutes: 36,
				Seconds: 14,
			},
			longDMS: DMS{
				Degrees: -181,
				Minutes: 0,
				Seconds: 0,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &LatLonDMS{
				Latitude:  tt.latDMS,
				Longitude: tt.longDMS,
			}
			err := a.Valid()
			if (err != nil) != tt.wantErr {
				t.Errorf("Valid() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
