package location

import "testing"

// BenchmarkToLatLonDMS benchmarks the LatLonDMS function.
func BenchmarkToLatLonDMS(b *testing.B) {
	latLonDD := LatLonDD{
		Latitude:  46.60416666333334,
		Longitude: 15.625000003333334,
	}

	for i := 0; i < b.N; i++ {
		_ = ConvertLatLonDDToLatLonDMS(latLonDD)
	}
}

// BenchmarkToLatLonDMS benchmarks the LatLonDMS function.
func BenchmarkDMSToLatLon(b *testing.B) {
	latLonDMS := LatLonDMS{
		Latitude: DMS{
			Degrees: 46,
			Minutes: 36,
			Seconds: 14,
		},
		Longitude: DMS{
			Degrees: 15,
			Minutes: 37,
			Seconds: 30,
		},
	}

	for i := 0; i < b.N; i++ {
		_, _ = ConvertLatLonDMStoLatLonDD(latLonDMS)
	}
}

func BenchmarkConvertDecimalDegreeToDMS(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ConvertDDtoDMS(43.7325)
	}
}

func BenchmarkLatLonDDtoString(b *testing.B) {
	latLonDD := LatLonDD{
		Latitude:  -1,
		Longitude: -1,
	}
	for i := 0; i < b.N; i++ {
		_ = latLonDD.String()
	}
}

func BenchmarkDMStoString(b *testing.B) {
	latLonDMS := LatLonDMS{
		Latitude: DMS{
			Degrees: 10,
			Minutes: 0,
			Seconds: 0,
		},
		Longitude: DMS{
			Degrees: 10,
			Minutes: 0,
			Seconds: 0,
		},
	}
	for i := 0; i < b.N; i++ {
		_ = latLonDMS.String()
	}
}

func BenchmarkConvertDMStoDD(b *testing.B) {
	var dms = DMS{
		Degrees: 0,
		Minutes: 0,
		Seconds: 0,
	}
	for i := 0; i < b.N; i++ {
		_ = ConvertDMStoDD(dms)
	}
}

func BenchmarkConvertDDtoDMS(b *testing.B) {
	DD := 0.1
	for i := 0; i < b.N; i++ {
		_ = ConvertDDtoDMS(DD)
	}

}

func BenchmarkConvertLocatorToLatLonDD(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, _ = ConvertLocatorToLatLonDD("JN76TO")
	}
}
