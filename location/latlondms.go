package location

import (
	"fmt"
)

// LatLonDMS represents geographical coordinates using degrees, minutes, and seconds.
// Latitude: Positive values indicate North, negative values indicate South, ranging from -90 to 90
// Longitude: Positive values indicate East, negative values indicate West, -180 to 180
type LatLonDMS struct {
	Latitude  DMS `json:"latitude"`
	Longitude DMS `json:"longitude"`
}

// Valid checks the validity of the LatLonDMS coordinates by validating both the latitude and longitude components.
func (a LatLonDMS) Valid() error {
	if err := a.Latitude.Valid(true); err != nil {
		return fmt.Errorf("latitude: %s", err)
	}
	if err := a.Longitude.Valid(false); err != nil {
		return fmt.Errorf("longitude: %s", err)
	}
	return nil
}

// StringNew returns the LatLonDMS coordinates as a formatted string in
// degrees, minutes, and seconds (DMS) format, with the appropriate
// hemisphere indicators (N/S for latitude and E/W for longitude).
func (a LatLonDMS) String() string {
	var NS, EW string

	latDeg := a.Latitude.Degrees
	lonDeg := a.Longitude.Degrees

	if latDeg < 0 {
		NS = "S"
		latDeg = -latDeg
	} else {
		NS = "N"
	}
	if lonDeg < 0 {
		EW = "W"
		lonDeg = -lonDeg
	} else {
		EW = "E"
	}

	return fmt.Sprintf(`%d°%d'%.2f"%s %d°%d'%.2f"%s`,
		latDeg, a.Latitude.Minutes, a.Latitude.Seconds, NS,
		lonDeg, a.Longitude.Minutes, a.Longitude.Seconds, EW)
}

// DMS represents Degrees, Minutes, and Seconds used for geographical coordinate
// Components of DMS:
// Degrees: The primary unit of measurement, ranging from -90 to 90 for latitude and -180 to 180 for longitude.
// Minutes: Each degree is divided into 60 minutes.
// Seconds: Each minute is further divided into 60 seconds.
type DMS struct {
	Degrees int     `json:"degrees"`
	Minutes int     `json:"minutes"`
	Seconds float64 `json:"seconds"`
}

// Valid checks the validity of the DMS coordinates. If isLatitude is true, it validates latitude constraints.
// If isLatitude is false, it validates longitude constraints. Returns an error if any validation fails.
func (a DMS) Valid(isLatitude bool) error {
	if isLatitude {
		if a.Degrees < -90 || a.Degrees > 90 {
			return fmt.Errorf("isLatitude degrees must be between -90 and 90")
		}
	} else {
		if a.Degrees < -180 || a.Degrees > 180 {
			return fmt.Errorf("longitude degrees must be between -180 and 180")
		}
	}
	if a.Minutes < 0 || a.Minutes >= 60 {
		return fmt.Errorf("minutes must be between 0 and less than 60")
	}
	if a.Seconds < 0 || a.Seconds >= 60 {
		return fmt.Errorf("seconds must be between 0 and less than 60")
	}
	return nil
}

// String returns a human-readable representation of the DMS coordinate in the format "degrees° minutes' seconds".
func (a DMS) String() string {
	return fmt.Sprintf(`%d°%d'%.2f"`, a.Degrees, a.Minutes, a.Seconds)
}
