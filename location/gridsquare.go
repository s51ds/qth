package location

import (
	"errors"
	"fmt"
)

// GridSquare represents a named geographical area defined by its corner and center coordinates.
type GridSquare struct {
	Name string
	A    LatLonDD // A represents the lower left geographical coordinate
	B    LatLonDD // B represents the upper left geographical coordinate
	C    LatLonDD // C represents the upper right geographical coordinate.
	D    LatLonDD // D represents the lower right geographical coordinate
	X    LatLonDD // X center
}

// Equals compares two GridSquare instances and returns true if they have the same name and identical coordinates.
func (a GridSquare) Equals(b GridSquare) bool {
	return a.String() == b.String()
}

// String returns a formatted string representation of the GridSquare including its name and geographical coordinates.
func (a GridSquare) String() string {
	// fmt.Println("X", a.X.LatLonDMS().String())
	// fmt.Println("A", a.A.LatLonDMS().String())
	// fmt.Println("B", a.B.LatLonDMS().String())
	// fmt.Println("C", a.C.LatLonDMS().String())
	// fmt.Println("D", a.D.LatLonDMS().String())
	// fmt.Println()
	s1 := fmt.Sprintf("%s [%s]", a.Name, a.X.String())
	s2 := fmt.Sprintf("B=[%s], C=[%s]", a.B.String(), a.C.String())
	s3 := fmt.Sprintf("A=[%s], D=[%s]", a.A.String(), a.D.String())
	return fmt.Sprintf("%s\n%s\n%s\n", s1, s2, s3)
}

func GetLocatorGridSquare(locator string) (gs GridSquare, err error) {
	gs.Name = locator
	if gs.X, err = ConvertLocatorToLatLonDD(locator); err != nil {
		return gs, err
	}
	switch len(locator) {
	case 2:
		gs.A.Latitude = gs.X.Latitude - 5
		gs.A.Longitude = gs.X.Longitude - 10
		gs.B.Latitude = gs.X.Latitude + 5
		gs.B.Longitude = gs.X.Longitude - 10
		gs.C.Latitude = gs.X.Latitude + 5
		gs.C.Longitude = gs.X.Longitude + 10
		gs.D.Latitude = gs.X.Latitude - 5
		gs.D.Longitude = gs.X.Longitude + 10
		return gs, nil
	case 4:
		gs.A.Latitude = gs.X.Latitude - 0.5
		gs.A.Longitude = gs.X.Longitude - 1
		gs.B.Latitude = gs.X.Latitude + 0.5
		gs.B.Longitude = gs.X.Longitude - 1
		gs.C.Latitude = gs.X.Latitude + 0.5
		gs.C.Longitude = gs.X.Longitude + 1
		gs.D.Latitude = gs.X.Latitude - 0.5
		gs.D.Longitude = gs.X.Longitude + 1
		return gs, nil
	case 6:
		gs.A.Latitude = gs.X.Latitude - 0.02083333   // 1.25' / 60
		gs.A.Longitude = gs.X.Longitude - 0.04166667 // 2.5' / 60
		gs.B.Latitude = gs.X.Latitude + 0.02083333
		gs.B.Longitude = gs.X.Longitude - 0.04166667
		gs.C.Latitude = gs.X.Latitude + 0.02083333
		gs.C.Longitude = gs.X.Longitude + 0.04166667
		gs.D.Latitude = gs.X.Latitude - 0.02083333
		gs.D.Longitude = gs.X.Longitude + 0.04166667
		return gs, nil
	}
	return gs, errors.New(fmt.Sprintf("Illegal argumet value! qthLocator=%s", locator))
}
