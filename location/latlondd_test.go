package location

import (
	"testing"
)

func TestLatLon_String(t *testing.T) {
	type fields struct {
		Latitude  float64
		Longitude float64
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "0",
			fields: fields{
				Latitude:  0,
				Longitude: 0,
			},
			want: "0.000000N 0.000000E",
		},
		{
			name: "S59ABC",
			fields: fields{
				Latitude:  46.60416666333334,
				Longitude: 15.625000003333334,
			},
			want: "46.604167N 15.625000E",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &LatLonDD{
				Latitude:  tt.fields.Latitude,
				Longitude: tt.fields.Longitude,
			}
			if got := a.String(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLatLon_String2dec(t *testing.T) {
	type fields struct {
		Latitude  float64
		Longitude float64
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "0",
			fields: fields{
				Latitude:  0,
				Longitude: 0,
			},
			want: "0.00N 0.00E",
		},
		{
			name: "S59ABC",
			fields: fields{
				Latitude:  46.60416666333334,
				Longitude: 15.625000003333334,
			},
			want: "46.60N 15.63E",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &LatLonDD{
				Latitude:  tt.fields.Latitude,
				Longitude: tt.fields.Longitude,
			}
			if got := a.String2dec(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLatLon_Equal(t *testing.T) {
	type fields struct {
		Latitude  float64
		Longitude float64
	}
	type args struct {
		b LatLonDD
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "true-1",
			fields: fields{
				Latitude:  0,
				Longitude: 0,
			},
			args: args{
				b: LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
			},
			want: true,
		},
		{
			name: "true-2",
			fields: fields{
				Latitude:  46.604167,
				Longitude: 15.625,
			},
			args: args{
				b: LatLonDD{
					Latitude:  46.60416666333334,
					Longitude: 15.625000003333334,
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &LatLonDD{
				Latitude:  tt.fields.Latitude,
				Longitude: tt.fields.Longitude,
			}
			if got := a.Equals(tt.args.b); got != tt.want {
				t.Errorf("Equals() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLatLon_Valid(t *testing.T) {
	type fields struct {
		Latitude  float64
		Longitude float64
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "ValidCoordinates-1",
			fields: fields{
				Latitude:  90,
				Longitude: 180,
			},
			wantErr: false,
		},
		{
			name: "ValidCoordinates-2",
			fields: fields{
				Latitude:  -90,
				Longitude: -180,
			},
			wantErr: false,
		},

		{
			name: "InvalidLatitude-1",
			fields: fields{
				Latitude:  90.000001,
				Longitude: 90,
			},
			wantErr: true,
		},
		{
			name: "InvalidLongitude-1",
			fields: fields{
				Latitude:  45,
				Longitude: 180.00001,
			},
			wantErr: true,
		},
		{
			name: "InvalidLatitude-2",
			fields: fields{
				Latitude:  -90.000001,
				Longitude: 90,
			},
			wantErr: true,
		},
		{
			name: "InvalidLongitude-2",
			fields: fields{
				Latitude:  45,
				Longitude: -180.00001,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &LatLonDD{
				Latitude:  tt.fields.Latitude,
				Longitude: tt.fields.Longitude,
			}
			if err := a.Valid(); (err != nil) != tt.wantErr {
				t.Errorf("Valid() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
