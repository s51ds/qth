package location

import (
	"fmt"
	"math"
	"reflect"
	"strconv"
	"strings"
	"testing"
)

func TestLatLonFromLocator(t *testing.T) {
	type args struct {
		locator string
	}
	tests := []struct {
		name       string
		args       args
		wantLatLon LatLonDD
		wantErr    bool
	}{
		{
			name: "S59ABC-JN76TO",
			args: args{
				locator: "JN76TO",
			},
			wantLatLon: LatLonDD{
				Latitude:  46.60379,
				Longitude: 15.60522,
			},
			wantErr: false,
		},
		{
			name: "K1TTT-FN32LL",
			args: args{
				locator: "FN32LL",
			},
			wantLatLon: LatLonDD{
				Latitude:  42.47916666333334,
				Longitude: -73.04166666333333,
			},
			wantErr: false,
		},
		{
			name: "PS2T-GG58WG",
			args: args{
				locator: "GG58WG",
			},
			wantLatLon: LatLonDD{
				Latitude:  -21.72916667,
				Longitude: -48.124999996666666,
			},
			wantErr: false,
		},
		{
			name: "ZM4T-RF80LQ",
			args: args{
				locator: "RF80LQ",
			},
			wantLatLon: LatLonDD{
				Latitude:  -39.312500003333334,
				Longitude: 176.95833333666667,
			},
			wantErr: false,
		},
		//
		// //////
		//
		// TODO; lat lon to loc
		{
			name: "RA90xa",
			args: args{
				locator: "RA90xa",
			},
			wantLatLon: LatLonDD{
				Latitude:  -89.98,
				Longitude: 179.96,
			},
			wantErr: false,
		},
		{
			name: "RR99XX",
			args: args{
				locator: "RR99XX",
			},
			wantLatLon: LatLonDD{
				Latitude:  89.98,
				Longitude: 179.96,
			},
			wantErr: false,
		},
		{
			name: "AA00aa",
			args: args{
				locator: "AA00aa",
			},
			wantLatLon: LatLonDD{
				Latitude:  -89.98,
				Longitude: -179.96,
			},
			wantErr: false,
		},
		{
			name: "AR09ax",
			args: args{
				locator: "AR09ax",
			},
			wantLatLon: LatLonDD{
				Latitude:  89.98,
				Longitude: -179.96,
			},
			wantErr: false,
		},
		//
		// /////
		{
			name: "RR09AX",
			args: args{
				locator: "RR09AX",
			},
			wantLatLon: LatLonDD{
				Latitude:  89.98,
				Longitude: 160.04,
			},
			wantErr: false,
		},
		{
			name: "AA00AA",
			args: args{
				locator: "AA00AA",
			},
			wantLatLon: LatLonDD{
				Latitude:  -89.98,
				Longitude: -179.96,
			},
			wantErr: false,
		},
		{
			name: "RA90XA",
			args: args{
				locator: "RA90XA",
			},
			wantLatLon: LatLonDD{
				Latitude:  -89.98,
				Longitude: 179.98,
			},
			wantErr: false,
		},
		{
			name: "AA00AA",
			args: args{
				locator: "AA00AA",
			},
			wantLatLon: LatLonDD{
				Latitude:  -89.98,
				Longitude: -179.96,
			},
			wantErr: false,
		},
		// //////
		// N/S pole
		{
			name: "North-pole-1",
			args: args{
				locator: "AR09AX",
			},
			wantLatLon: LatLonDD{
				Latitude:  89.97916666,
				Longitude: -179.95833,
			},
			wantErr: false,
		},
		{
			name: "North-pole-2",
			args: args{
				locator: "RR99XX",
			},
			wantLatLon: LatLonDD{
				Latitude:  89.979166,
				Longitude: 179.9583333,
			},
			wantErr: false,
		},
		{
			name: "South-pole-1",
			args: args{
				locator: "AA00AA",
			},
			wantLatLon: LatLonDD{
				Latitude:  -89.98,
				Longitude: -179.96,
			},
			wantErr: false,
		},
		{
			name: "South-pole-2",
			args: args{
				locator: "RA90XA",
			},
			wantLatLon: LatLonDD{
				Latitude:  -89.98,
				Longitude: 179.96,
			},
			wantErr: false,
		},
		{
			name: "JN",
			args: args{
				locator: "JN",
			},
			wantLatLon: LatLonDD{
				Latitude:  45,
				Longitude: 10,
			},
			wantErr: false,
		},
		{
			name: "JN76",
			args: args{
				locator: "JN76",
			},
			wantLatLon: LatLonDD{
				Latitude:  46.5,
				Longitude: 15,
			},
			wantErr: false,
		},

		{
			name: "JO",
			args: args{
				locator: "JO",
			},
			wantLatLon: LatLonDD{
				Latitude:  55,
				Longitude: 10,
			},
			wantErr: false,
		},
		{
			name: "KN",
			args: args{
				locator: "KN",
			},
			wantLatLon: LatLonDD{
				Latitude:  45,
				Longitude: 30,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			latLon, err := ConvertLocatorToLatLonDD(tt.args.locator)
			if (err != nil) != tt.wantErr {
				t.Errorf("ConvertLocatorToLatLonDD() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if fmt.Sprintf("%0.1f", latLon.Latitude) != fmt.Sprintf("%0.1f", tt.wantLatLon.Latitude) || fmt.Sprintf("%0.1f", latLon.Longitude) != fmt.Sprintf("%0.1f", tt.wantLatLon.Longitude) {
				t.Errorf("ConvertLocatorToLatLonDD() latLon = %v, want %v", latLon, tt.wantLatLon)
			}
		})
	}
}

func TestLocatorFromLatLon(t *testing.T) {
	type args struct {
		latLon LatLonDD
	}
	tests := []struct {
		name        string
		args        args
		wantLocator string
		wantErr     bool
	}{
		// ///////
		//
		{
			name: "RA90xa",
			args: args{
				latLon: LatLonDD{
					Latitude:  -89.98,
					Longitude: 179.96,
				},
			},
			wantLocator: "RA90XA",
			wantErr:     false,
		},
		{
			name: "RR99XX",
			args: args{
				latLon: LatLonDD{
					Latitude:  89.98,
					Longitude: 179.96,
				},
			},
			wantLocator: "RR99XX",
			wantErr:     false,
		},
		{
			name: "AA00aa",
			args: args{
				latLon: LatLonDD{
					Latitude:  -89.98,
					Longitude: -179.96,
				},
			},
			wantLocator: "AA00AA",
			wantErr:     false,
		},
		{
			name: "AR09ax",
			args: args{
				latLon: LatLonDD{
					Latitude:  89.98,
					Longitude: -179.96,
				},
			},
			wantLocator: "AR09AX",
			wantErr:     false,
		},

		//
		// //////
		{
			name: "err-1",
			args: args{
				latLon: LatLonDD{
					Latitude:  91,
					Longitude: 0,
				},
			},
			wantLocator: "",
			wantErr:     true,
		},
		{
			name: "err-2",
			args: args{
				latLon: LatLonDD{
					Latitude:  -91,
					Longitude: 0,
				},
			},
			wantLocator: "",
			wantErr:     true,
		},
		{
			name: "err-3",
			args: args{
				latLon: LatLonDD{
					Latitude:  0,
					Longitude: 181,
				},
			},
			wantLocator: "",
			wantErr:     true,
		},
		{
			name: "err-4",
			args: args{
				latLon: LatLonDD{
					Latitude:  0,
					Longitude: -181,
				},
			},
			wantLocator: "",
			wantErr:     true,
		},
		// //////////////////////////
		// ok cases
		{
			name: "0 0",
			args: args{
				latLon: LatLonDD{
					Latitude:  0,
					Longitude: 0,
				},
			},
			wantLocator: "JJ00AA",
			wantErr:     false,
		},
		{
			name: "45 0",
			args: args{
				latLon: LatLonDD{
					Latitude:  45,
					Longitude: 0,
				},
			},
			wantLocator: "JN05AA",
			wantErr:     false,
		},
		{
			name: "-45 0",
			args: args{
				latLon: LatLonDD{
					Latitude:  -45,
					Longitude: 0,
				},
			},
			wantLocator: "JE05AA",
			wantErr:     false,
		},
		{
			name: "-85 0",
			args: args{
				latLon: LatLonDD{
					Latitude:  -85,
					Longitude: 0,
				},
			},
			wantLocator: "JA05AA",
			wantErr:     false,
		},

		{
			name: "0 90",
			args: args{
				latLon: LatLonDD{
					Latitude:  0,
					Longitude: 90,
				},
			},
			wantLocator: "NJ50AA",
			wantErr:     false,
		},
		{
			name: "45 90",
			args: args{
				latLon: LatLonDD{
					Latitude:  45,
					Longitude: 90,
				},
			},
			wantLocator: "NN55AA",
			wantErr:     false,
		},
		{
			name: "85 90",
			args: args{
				latLon: LatLonDD{
					Latitude:  85,
					Longitude: 90,
				},
			},
			wantLocator: "NR55AA",
			wantErr:     false,
		},
		{
			name: "-45 90",
			args: args{
				latLon: LatLonDD{
					Latitude:  -45,
					Longitude: 90,
				},
			},
			wantLocator: "NE55AA",
			wantErr:     false,
		},
		{
			name: "-85 90",
			args: args{
				latLon: LatLonDD{
					Latitude:  -85,
					Longitude: 90,
				},
			},
			wantLocator: "NA55AA",
			wantErr:     false,
		},

		{
			name: "0 179.9999999",
			args: args{
				latLon: LatLonDD{
					Latitude:  0,
					Longitude: 179.9999999,
				},
			},
			wantLocator: "RJ90XA",
			wantErr:     false,
		},
		{
			name: "45 179.9999999",
			args: args{
				latLon: LatLonDD{
					Latitude:  45,
					Longitude: 179.9999999,
				},
			},
			wantLocator: "RN95XA",
			wantErr:     false,
		},
		{
			name: "85 179.9999999",
			args: args{
				latLon: LatLonDD{
					Latitude:  85,
					Longitude: 179.9999999,
				},
			},
			wantLocator: "RR95XA",
			wantErr:     false,
		},
		{
			name: "-45 179.9999999",
			args: args{
				latLon: LatLonDD{
					Latitude:  -45,
					Longitude: 179.9999999,
				},
			},
			wantLocator: "RE95XA",
			wantErr:     false,
		},
		{
			name: "0 -90",
			args: args{
				latLon: LatLonDD{
					Latitude:  0,
					Longitude: -90,
				},
			},
			wantLocator: "EJ50AA",
			wantErr:     false,
		},
		{
			name: "45 -90",
			args: args{
				latLon: LatLonDD{
					Latitude:  45,
					Longitude: -90,
				},
			},
			wantLocator: "EN55AA",
			wantErr:     false,
		},

		{
			name: "0 180",
			args: args{
				latLon: LatLonDD{
					Latitude:  0,
					Longitude: -179.99999,
				},
			},
			wantLocator: "AJ00AA",
			wantErr:     false,
		},
		{
			name: "45 180",
			args: args{
				latLon: LatLonDD{
					Latitude:  45,
					Longitude: -179.99999,
				},
			},
			wantLocator: "AN05AA",
			wantErr:     false,
		},
		{
			name: "85 180",
			args: args{
				latLon: LatLonDD{
					Latitude:  85,
					Longitude: -179.99999,
				},
			},
			wantLocator: "AR05AA",
			wantErr:     false,
		},
		{
			name: "-45 180",
			args: args{
				latLon: LatLonDD{
					Latitude:  -45,
					Longitude: -179.99999,
				},
			},
			wantLocator: "AE05AA",
			wantErr:     false,
		},
		{
			name: "-85 180",
			args: args{
				latLon: LatLonDD{
					Latitude:  -85,
					Longitude: -179.99999,
				},
			},
			wantLocator: "AA05AA",
			wantErr:     false,
		},

		// /////////////
		//
		{
			name: "S59ABC-JN76TO",
			args: args{
				latLon: LatLonDD{
					Latitude:  46.60379,
					Longitude: 15.60522,
				},
			},
			wantLocator: "JN76TO",
			wantErr:     false,
		},
		{
			name: "K1TTT-FN32LL",
			args: args{
				latLon: LatLonDD{
					Latitude:  42.47916666333334,
					Longitude: -73.04166666333333,
				},
			},
			wantLocator: "FN32LL",
			wantErr:     false,
		},
		{
			name: "PS2T-GG58WG",
			args: args{
				latLon: LatLonDD{
					Latitude:  -21.72916667,
					Longitude: -48.124999996666666,
				},
			},
			wantLocator: "GG58WG",
			wantErr:     false,
		},
		{
			name: "ZM4T-RF80LQ",
			args: args{
				latLon: LatLonDD{
					Latitude:  -39.312500003333334,
					Longitude: 176.95833333666667,
				},
			},
			wantLocator: "RF80LQ",
			wantErr:     false,
		},

		// //////
		// N/S pole
		{
			name: "North-pole-1",
			args: args{
				latLon: LatLonDD{
					Latitude:  89.99999999999999,
					Longitude: -179.999999999999,
				},
			},
			wantLocator: "AR09AX",
			wantErr:     false,
		},
		{
			name: "North-pole-2",
			args: args{
				latLon: LatLonDD{
					Latitude:  89.99999999999999,
					Longitude: 179.999999999999,
				},
			},
			wantLocator: "RR99XX",
			wantErr:     false,
		},
		{
			name: "South-pole-1",
			args: args{
				latLon: LatLonDD{
					Latitude:  -89.99999999999999,
					Longitude: -179.999999999999,
				},
			},
			wantLocator: "AA00AA",
			wantErr:     false,
		},
		{
			name: "South-pole-2",
			args: args{
				latLon: LatLonDD{
					Latitude:  -89.99999999999999,
					Longitude: 179.999999999999,
				},
			},
			wantLocator: "RA90XA",
			wantErr:     false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotLocator, err := ConvertLatLonDDtoLocator(tt.args.latLon)
			if (err != nil) != tt.wantErr {
				t.Errorf("ConvertLatLonDDtoLocator() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotLocator != tt.wantLocator {
				t.Errorf("ConvertLatLonDDtoLocator() gotLocator = %v, want %v", gotLocator, tt.wantLocator)
			}
		})
	}
}

func TestConvertDecimalDegreeToDMS(t *testing.T) {
	type args struct {
		decimalDegree float64
	}
	tests := []struct {
		name string
		args args
		want DMS
	}{
		{
			name: "90 deg min 58 sec 59.9999",
			args: args{
				decimalDegree: 90.9833333055555555,
			},
			want: DMS{
				Degrees: 90,
				Minutes: 59,
				Seconds: 0,
			},
		},
		{
			name: "179 deg min 58 sec 59.9999",
			args: args{
				decimalDegree: 179.983333,
			},
			want: DMS{
				Degrees: 179,
				Minutes: 59,
				Seconds: 0,
			},
		},

		{
			name: "min 58 sec 59.9999",
			args: args{
				decimalDegree: 0.9833333055555555,
			},
			want: DMS{
				Degrees: 0,
				Minutes: 59,
				Seconds: 0,
			},
		},
		{
			name: "min 59 sec 59.9999",
			args: args{
				decimalDegree: 0.9999999722222221,
			},
			want: DMS{
				Degrees: 1,
				Minutes: 0,
				Seconds: 0,
			},
		},
		{
			name: "sec 59.9999-1",
			args: args{
				decimalDegree: 0.016666638888,
			},
			want: DMS{
				Degrees: 0,
				Minutes: 1,
				Seconds: 0,
			},
		},
		{
			name: "0",
			args: args{
				decimalDegree: 0,
			},
			want: DMS{
				Degrees: 0,
				Minutes: 0,
				Seconds: 0,
			},
		},
		{
			name: "0.99999999999999999",
			args: args{
				decimalDegree: 0.99999999999999999,
			},
			want: DMS{
				Degrees: 1,
				Minutes: 0,
				Seconds: 0,
			},
		},
		{
			name: "-0.99999999999999999",
			args: args{
				decimalDegree: -0.99999999999999999,
			},
			want: DMS{
				Degrees: -1,
				Minutes: 0,
				Seconds: 0,
			},
		},
		{
			name: "179.99999999999999999",
			args: args{
				decimalDegree: 179.99999999999999999,
			},
			want: DMS{
				Degrees: 180,
				Minutes: 0,
				Seconds: 0,
			},
		},
		{
			name: "-179.99999999999999999",
			args: args{
				decimalDegree: -179.99999999999999999,
			},
			want: DMS{
				Degrees: -180,
				Minutes: 0,
				Seconds: 0,
			},
		},

		{ //  https://manoa.hawaii.edu/exploringourfluidearth/physical/world-ocean/locating-points-globe/compare-contrast-connect-converting-decimal-degrees
			name: "Location of Titanic: 49.947 decimal degrees west",
			args: args{
				decimalDegree: -49.947,
			},
			want: DMS{
				Degrees: -49,
				Minutes: 56,
				Seconds: 49.00,
			},
		},
		{ //  https://manoa.hawaii.edu/exploringourfluidearth/physical/world-ocean/locating-points-globe/compare-contrast-connect-converting-decimal-degrees
			name: "Location of Titanic: 43.7325 decimal degrees north",
			args: args{
				decimalDegree: 43.7325,
			},
			want: DMS{
				Degrees: 43,
				Minutes: 43,
				Seconds: 57,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ConvertDDtoDMS(tt.args.decimalDegree); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ConvertDDtoDMS() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestConvertLatLonDMStoLatLonDD(t *testing.T) {
	type args struct {
		latLonDMS LatLonDMS
	}
	tests := []struct {
		name         string
		args         args
		wantLatLonDD LatLonDD
		wantErr      bool
	}{
		{
			name: "0",
			args: args{
				latLonDMS: LatLonDMS{
					Latitude: DMS{
						Degrees: 0,
						Minutes: 0,
						Seconds: 0,
					},
					Longitude: DMS{
						Degrees: 0,
						Minutes: 0,
						Seconds: 0,
					},
				},
			},
			wantLatLonDD: LatLonDD{
				Latitude:  0,
				Longitude: 0,
			},
			wantErr: false,
		},
		{
			name: "S59ABC",
			args: args{
				latLonDMS: LatLonDMS{
					Latitude: DMS{
						Degrees: 46,
						Minutes: 36,
						Seconds: 15,
					},
					Longitude: DMS{
						Degrees: 15,
						Minutes: 37,
						Seconds: 30,
					},
				},
			},
			wantLatLonDD: LatLonDD{
				Latitude:  46.604167,
				Longitude: 15.625000,
			},
			wantErr: false,
		},
		{
			name: "err",
			args: args{
				latLonDMS: LatLonDMS{
					Latitude: DMS{
						Degrees: 170,
						Minutes: 0,
						Seconds: 0,
					},
					Longitude: DMS{
						Degrees: 0,
						Minutes: 0,
						Seconds: 0,
					},
				},
			},
			wantLatLonDD: LatLonDD{
				Latitude:  0,
				Longitude: 0,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotLatLonDD, err := ConvertLatLonDMStoLatLonDD(tt.args.latLonDMS)
			if (err != nil) != tt.wantErr {
				t.Errorf("ConvertLatLonDMStoLatLonDD() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotLatLonDD.String() != tt.wantLatLonDD.String() {
				t.Errorf("ConvertLatLonDMStoLatLonDD() gotLatLonDD = %v, want %v", gotLatLonDD, tt.wantLatLonDD)
				return
			}
		})
	}
}

func TestConvertLatLonDMStoLocator(t *testing.T) {
	type args struct {
		latLonDMS LatLonDMS
	}
	tests := []struct {
		name        string
		args        args
		wantLocator string
		wantErr     bool
	}{
		{
			name: "S59ABC",
			args: args{
				latLonDMS: LatLonDMS{
					Latitude: DMS{
						Degrees: 46,
						Minutes: 36,
						Seconds: 15,
					},
					Longitude: DMS{
						Degrees: 15,
						Minutes: 37,
						Seconds: 30,
					},
				},
			},
			wantLocator: "JN76TO",
			wantErr:     false,
		},
		{
			name: "err",
			args: args{
				latLonDMS: LatLonDMS{
					Latitude: DMS{
						Degrees: 91,
						Minutes: 0,
						Seconds: 0,
					},
					Longitude: DMS{
						Degrees: 0,
						Minutes: 0,
						Seconds: 0,
					},
				},
			},
			wantLocator: "",
			wantErr:     true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotLocator, err := ConvertLatLonDMStoLocator(tt.args.latLonDMS)
			if (err != nil) != tt.wantErr {
				t.Errorf("ConvertLatLonDMStoLocator() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotLocator != tt.wantLocator {
				t.Errorf("ConvertLatLonDMStoLocator() gotLocator = %v, want %v", gotLocator, tt.wantLocator)
			}
		})
	}
}

func TestConvertLocatorToLatLonDMS(t *testing.T) {
	type args struct {
		locator string
	}
	tests := []struct {
		name          string
		args          args
		wantLatLonDMS LatLonDMS
		wantErr       bool
	}{
		{
			name: "S59ABC",
			args: args{
				locator: "JN76TO",
			},
			wantLatLonDMS: LatLonDMS{
				Latitude: DMS{
					Degrees: 46,
					Minutes: 36,
					Seconds: 15,
				},
				Longitude: DMS{
					Degrees: 15,
					Minutes: 37,
					Seconds: 30,
				},
			},
			wantErr: false,
		},
		{
			name: "err-1",
			args: args{
				locator: "",
			},
			wantLatLonDMS: LatLonDMS{
				Latitude: DMS{
					Degrees: 0,
					Minutes: 0,
					Seconds: 0,
				},
				Longitude: DMS{
					Degrees: 0,
					Minutes: 0,
					Seconds: 0,
				},
			},
			wantErr: true,
		},
		{
			name: "err-2",
			args: args{
				locator: "s59abc",
			},
			wantLatLonDMS: LatLonDMS{
				Latitude: DMS{
					Degrees: 0,
					Minutes: 0,
					Seconds: 0,
				},
				Longitude: DMS{
					Degrees: 0,
					Minutes: 0,
					Seconds: 0,
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotLatLonDMS, err := ConvertLocatorToLatLonDMS(tt.args.locator)
			if (err != nil) != tt.wantErr {
				t.Errorf("ConvertLocatorToLatLonDMS() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotLatLonDMS, tt.wantLatLonDMS) {
				t.Errorf("ConvertLocatorToLatLonDMS() gotLatLonDMS = %v, want %v", gotLatLonDMS, tt.wantLatLonDMS)
			}
		})
	}
}

func TestConvertLatLonDDToLatLonDMS(t *testing.T) {
	type fields struct {
		Latitude  float64
		Longitude float64
	}
	tests := []struct {
		name   string
		fields fields
		want   LatLonDMS
	}{
		{
			name: "0",
			fields: fields{
				Latitude:  0,
				Longitude: 0,
			},
			want: LatLonDMS{
				Latitude: DMS{
					Degrees: 0,
					Minutes: 0,
					Seconds: 0,
				},
				Longitude: DMS{
					Degrees: 0,
					Minutes: 0,
					Seconds: 0,
				},
			},
		},
		{
			name: "S59ABC",
			fields: fields{
				Latitude:  46.60416666333334,
				Longitude: 15.625000003333334,
			},
			want: LatLonDMS{
				Latitude: DMS{
					Degrees: 46,
					Minutes: 36,
					Seconds: 15,
				},
				Longitude: DMS{
					Degrees: 15,
					Minutes: 37,
					Seconds: 30,
				},
			},
		},
		{
			name: "-S59ABC",
			fields: fields{
				Latitude:  -46.60416666333334,
				Longitude: -15.625000003333334,
			},
			want: LatLonDMS{
				Latitude: DMS{
					Degrees: -46,
					Minutes: 36,
					Seconds: 15,
				},
				Longitude: DMS{
					Degrees: -15,
					Minutes: 37,
					Seconds: 30,
				},
			},
		},
		{
			name: "LAX",
			fields: fields{
				Latitude:  33.95,
				Longitude: -118.4,
			},
			want: LatLonDMS{
				Latitude: DMS{
					Degrees: 33,
					Minutes: 57,
					Seconds: 0,
				},
				Longitude: DMS{
					Degrees: -118,
					Minutes: 24,
					Seconds: 0,
				},
			},
		},
		{
			name: "JFK",
			fields: fields{
				Latitude:  40.633333,
				Longitude: -73.783333,
			},
			want: LatLonDMS{
				Latitude: DMS{
					Degrees: 40,
					Minutes: 38,
					Seconds: 0,
				},
				Longitude: DMS{
					Degrees: -73,
					Minutes: 47,
					Seconds: 0,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &LatLonDD{
				Latitude:  tt.fields.Latitude,
				Longitude: tt.fields.Longitude,
			}
			if got := ConvertLatLonDDToLatLonDMS(*a); got.String() != tt.want.String() {
				t.Errorf("LatLonDMS() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_ConvertDMStoDecimalDegree(t *testing.T) {
	type args struct {
		dms DMS
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "0",
			args: args{
				dms: DMS{
					Degrees: 0,
					Minutes: 0,
					Seconds: 0,
				},
			},
			want: 0,
		},
		{
			name: "1",
			args: args{
				dms: DMS{
					Degrees: 1,
					Minutes: 0,
					Seconds: 0,
				},
			},
			want: 1,
		},
		{
			name: "-5",
			args: args{
				dms: DMS{
					Degrees: -5,
					Minutes: 42,
					Seconds: 0,
				},
			},
			want: -5.7,
		},
		{
			name: "sec 59.9999",
			args: args{
				dms: DMS{
					Degrees: 0,
					Minutes: 0,
					Seconds: 59.9999,
				},
			},
			want: 0.016666638888888887,
		},
		{
			name: "min 59 sec 59.9999",
			args: args{
				dms: DMS{
					Degrees: 0,
					Minutes: 59,
					Seconds: 59.9999,
				},
			},
			want: 0.9999999722222221,
		},
		{
			name: "min 58 sec 59.9999",
			args: args{
				dms: DMS{
					Degrees: 0,
					Minutes: 58,
					Seconds: 59.9999,
				},
			},
			want: 0.9833333055555555,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ConvertDMStoDD(tt.args.dms); got != tt.want {
				t.Errorf("ConvertDMStoDD() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLoopConvertDecimalDegreeToDMS(t *testing.T) {
	for d := -180; d <= 180; d++ {
		for m := 0; m < 60; m++ {
			// convert m to decimal minutes
			mf := float64(m) / 60.0
			// decimal part of float as string without dot
			mfs := fmt.Sprintf("%f", mf)
			ss := strings.Split(mfs, ".")
			s := fmt.Sprintf("%d.%s", d, ss[1])
			// convert s to float64
			f, _ := strconv.ParseFloat(s, 64)
			got := ConvertDDtoDMS(f)
			if got.Degrees != d {
				t.Errorf("ConvertDDtoDMS() = %v, want %v", got, d)
				return
			}

			if got.Minutes != m {
				if got.Seconds == 59.99 {
					if got.Minutes == m-1 {
						continue
					}
				}
				t.Errorf("ConvertDDtoDMS() = got minutes %d, want minutes %d, DMS=%s, DD=%f", got.Minutes, m, got.String(), f)
				return
			}
			if math.Abs(float64(got.Degrees)) > 180 {
				fmt.Println(got.String())
			}
		}
	}
}

func TestLoopSecConvertDecimalDegreeToDMS(t *testing.T) {
	for degree := -180; degree <= 180; degree++ {
		for minute := 0; minute < 60; minute++ {
			for second := 0; second < 60; second++ {
				secondFloat := float64(second) / 3600.0
				// convert minute to decimal minutes
				minuteFloat := float64(minute)/60.0 + secondFloat
				// decimal part of float as string without dot
				mfs := fmt.Sprintf("%f", minuteFloat)
				ss := strings.Split(mfs, ".")
				s := fmt.Sprintf("%d.%s", degree, ss[1])
				// convert s to float64
				f, _ := strconv.ParseFloat(s, 64)
				gotDMS := ConvertDDtoDMS(f)

				// fmt.Println(gotDMS.String(), degree, minute, second)

				if gotDMS.Seconds != float64(second) {
					t.Errorf("ConvertDDtoDMS() gotDMS seconds = %s, want %d", gotDMS, second)
					return
				}

				if gotDMS.Minutes != minute {
					if gotDMS.Seconds == 59.99 {
						if gotDMS.Minutes == minute-1 {
							continue
						}
					}
					t.Errorf("ConvertDDtoDMS() = gotDMS minutes %degree, want minutes %degree, DMS=%s, DD=%f", gotDMS.Minutes, minute, gotDMS.String(), f)
					return
				}

				if gotDMS.Degrees != degree {
					t.Errorf("ConvertDDtoDMS() gotDMS degrees = %s, want %d", gotDMS, degree)
					return
				}

				gotDD := ConvertDMStoDD(gotDMS)
				if gotDD-f > 0.000001 {
					t.Errorf("%f %f", gotDD, f)
					return
				}

				if math.Abs(gotDD) >= 180 {
					continue
				}

				// for i := 46.0; i < 47; i += 0.001 {
				// 	latLon := LatLonDD{Latitude: i, Longitude: gotDD}
				// 	if locator, err := ConvertLatLonDDtoLocator(latLon); err != nil {
				// 		t.Error(err)
				// 		return
				// 	} else {
				// 		if locator == "JN76TO" {
				// 			fmt.Println(locator, latLon.String())
				// 		}
				// 	}
				// }

			} // for second := 0; second < 60; second++ {
		} // for minute := 0; minute < 60; minute++
	} // for degree := -180; degree <= 180; degree++ {
}
