## QTH golang module

**QTH** provides functionality for calculating the distance between two locations on Earth.

The module name QTH originates from an international Q-code historically used in maritime communications to indicate a station’s location.
Today, it is primarily used in amateur radio, often alongside the Maidenhead locator system for precise location reporting.

### Location Input
Locations can be provided in the following forms:
- **Latitude/Longitude** in decimal degrees (DD) or degrees, minutes, and seconds (DMS)
- **Maidenhead QTH Locator** ([Maidenhead Locator System](https://en.wikipedia.org/wiki/Maidenhead_Locator_System))

### Conversion Functions
The project supports conversion between latitude/longitude and Maidenhead QTH locator in the **`location` package**:
- `ConvertLatLonDDtoLocator`: Converts latitude/longitude in decimal degrees to a Maidenhead QTH locator.
- `ConvertLatLonDMStoLocator`: Converts latitude/longitude in DMS format to a Maidenhead QTH locator.
- `ConvertLocatorToLatLonDD`: Converts a Maidenhead QTH locator to latitude/longitude in decimal degrees.
- `ConvertLocatorToLatLonDMS`: Converts a Maidenhead QTH locator to latitude/longitude in DMS format.

### Distance Calculation
The **`distance` package** supports calculating distances between two locations:
- `BetweenLatLonDD`: Calculates the distance between two points given in decimal degrees.
- `BetweenLatLonDMS`: Calculates the distance between two points given in DMS format.
- `BetweenQth`: Calculates the distance between two points given as Maidenhead QTH locators.

### Installation
To install the package:
```bash
go get gitlab.com/s51ds/qth
```

